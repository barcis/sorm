<?php

namespace SORM;

/**
 * Description of Model
 * @property int $id
 */
abstract class Model implements \ArrayAccess, \JsonSerializable {

    /**
     *
     * @var array
     */
    private static $extras = array();

    /**
     *
     * @var array
     */
    private static $calculateds = array();

    /**
     *
     * @var array
     */
    private static $columns = array();

    /**
     *
     * @var array
     */
    private static $constraints = array();

    /**
     *
     * @var array
     */
    private static $tables = array();

    /**
     *
     * @var array
     */
    private static $connections = array();

    /**
     *
     * @var array
     */
    private static $records;

    /**
     *
     * @var array
     */
    private static $virtuals;

    /**
     *
     * @var array
     */
    private $record = array();

    /**
     *
     * @var array
     */
    private $virtual = array();

    /**
     *
     * @var array
     */
    private $extra = array();

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var boolean
     */
    private $empty = true;

    /**
     *
     * @var boolean
     */
    private $preinit = true;

    /**
     *
     * @var boolean
     */
    private $change = false;

    /**
     *
     * @var array
     */
    private $changes = array();

    /**
     *
     * @var boolean
     */
    private $saving = false;

    /**
     *
     * @var string
     */
    private $createMode = '';

    /**
     *
     * @return boolean
     */
    public function isChanged() {
        return ((int) $this->id <= 0) || $this->change;
    }

    /**
     *
     * @return array
     */
    public function getChanges() {
        return $this->changes;
    }

    private static function define() {
        $class = static::getClass();
        if (!isset(self::$columns[$class])) {
            call_user_func([$class, '__definition']);
        }
    }

    protected static final function __table($table) {
        $class = static::getClass();
        self::$tables[$class] = $table;
    }

    protected static final function __conection($conection) {
        $class = static::getClass();
        self::$connections[$class] = $conection;
    }

    public static function __create_at() {
        self::addColumn(
                (new Type\Timestamp('create_at'))
                        ->setNotNull()
                        ->setDefaultValue('now()')
        );
    }

    public static function __update_at() {
        self::addColumn(
                (new Type\Timestamp('update_at'))
                        ->setNotNull()
                        ->setDefaultValue('now()')
        );
    }

    public static function __definition() {
        $class = static::getClass();

        self::$constraints[$class] = array(
            'uniques' => array(),
            'foreignkeys' => array(),
            'checks' => array()
        );
        self::$columns[$class] = array();
        self::$calculateds[$class] = array();

        self::addColumn(
                (new Type\Integer('id'))
                        ->setNotNull()
                        ->setAutoCRUD(
                                (new \SORM\AutoCRUD())
                                ->setLabel('#')
                                ->setOrder(1)
                        )
        );
    }

    /**
     *
     * @param string $name
     * @param callable $extra
     * @return callable
     */
    protected static function addExtra($name, callable $extra) {
        $class = static::getClass();

        self::$extras[$class][$name] = $extra;
        return self::$extras[$class][$name];
    }

    /**
     *
     * @return callable[]
     */
    public static function getExtras() {
        $class = static::getClass();

        if (!isset(self::$extras[$class])) {
            self::$extras[$class] = [];
        }

        return self::$extras[$class];
    }

    /**
     *
     * @param string $name
     * @return callable
     * @throws Exception\ColumnDoesNotExist
     */
    public static function getExtra($name) {
        $class = static::getClass();
        if (!isset(self::$extras[$class][$name])) {
            $class = static::getClass();
            throw new Exception\ColumnDoesNotExist($name, $class);
        }

        return self::$extras[$class][$name];
    }

    /**
     *
     * @return boolean
     */
    public static function isExtra($name) {
        $class = static::getClass();
        return isset(self::$extras[$class][$name]);
    }

    /**
     *
     * @param \SORM\Type $column
     * @return Query\Calculated
     */
    protected static function addCalculated($alias, $expression) {
        $class = static::getClass();

        self::$calculateds[$class][$alias] = new Query\Calculated($expression, $alias, self::getTableName());
        return self::$calculateds[$class][$alias];
    }

    /**
     *
     * @param string $alias
     * @return Query\Calculated[]
     * @throws Exception\ColumnDoesNotExist
     */
    public static function getCalculateds() {
        $class = static::getClass();
        return self::$calculateds[$class];
    }

    /**
     *
     * @param string $alias
     * @return Query\Calculated
     * @throws Exception\ColumnDoesNotExist
     */
    public static function getCalculated($alias) {
        $class = static::getClass();
        if (!isset(self::$calculateds[$class][$alias])) {
            $class = static::getClass();
            throw new Exception\ColumnDoesNotExist($alias, $class);
        }

        return self::$calculateds[$class][$alias];
    }

    /**
     *
     * @return boolean
     */
    public static function isCalculated($alias) {
        $class = static::getClass();
        return isset(self::$calculateds[$class][$alias]);
    }

    public static function removeColumn($name) {
        $class = static::getClass();

        if (isset(self::$columns[$class][$name])) {
            unset(self::$columns[$class][$name]);
        }
    }

    /**
     *
     * @param \SORM\Type $column
     */
    protected static function addColumn(Type $column) {
        $class = static::getClass();

        self::$columns[$class][$column->getName()] = $column;

        if ($column->getUnique()) {
            self::addConstraint(new Constraint\Unique($column));
        }

        if ($column->getForeignKeysOneToMany()) {
            foreach ($column->getForeignKeysOneToMany() as $oneToMany) {
                self::addConstraint($oneToMany);
            }
        }

        if ($column->getChecks()) {
            foreach ($column->getChecks() as $checks) {
                self::addConstraint($checks);
            }
        }
    }

    /**
     *
     * @param \SORM\Constraint $constraint
     */
    protected static function addConstraint(Constraint $constraint) {
        if ($constraint instanceof Constraint\Unique) {
            self::addUnique($constraint);
        } elseif ($constraint instanceof Constraint\ForeignKey) {
            self::addForeignKey($constraint);
        } elseif ($constraint instanceof Constraint\Check) {
            self::addCheck($constraint);
        }
    }

    /**
     *
     * @param \SORM\Constraint\Unique $unique
     */
    protected static function addUnique(Constraint\Unique $unique) {
        $class = static::getClass();
        $name = $unique->getName();
        self::$constraints[$class]['uniques'][$name] = $unique;
    }

    /**
     *
     * @param \SORM\Constraint\ForeignKey $foreignKey
     */
    protected static function addForeignKey(Constraint\ForeignKey $foreignKey) {
        $class = static::getClass();
        $name = $foreignKey->getField();
        self::$constraints[$class]['foreignkeys'][$name] = $foreignKey;
    }

    /**
     *
     * @param \SORM\Constraint\Check $check
     */
    protected static function addCheck(Constraint\Check $check) {
        $class = static::getClass();
        $name = $check->getName();
        self::$constraints[$class]['checks'][$name] = $check;
    }

    const FK_LOAD_NO_FK = 0;
    const FK_ONETOMANY = 1;
    const FK_MANYTOONE = 2;
    const FK_ALL = 9;

    /**
     *
     * @return Constraint\ForeignKey[]
     */
    public static function getForeignKeys($kind = self::FK_ALL, $k = false) {
        $class = static::getClass();
        $foreignkeys = self::$constraints[$class]['foreignkeys'];
        if ($k) {
            p($class, $foreignkeys);
        }
        return array_filter($foreignkeys, function($foreignkey) use ($kind) {
            if ($kind == Model::FK_ALL) {
                return true;
            } elseif ($kind == Model::FK_ONETOMANY && $foreignkey instanceof Constraint\ForeignKey\OneToMany) {
                return true;
            }
            return false;
        });
    }

    /**
     *
     * @return Constraint\ForeignKey
     */
    public static function getForeignKey($name) {
        $class = static::getClass();
        return self::$constraints[$class]['foreignkeys'][$name];
    }

    /**
     *
     * @return boolean
     */
    public static function isForeignKeys($name) {
        $class = static::getClass();
        return isset(self::$constraints[$class]['foreignkeys'][$name]);
    }

    /**
     *
     * @return Type[]
     */
    public static function getUniques() {
        $class = static::getClass();
        return self::$constraints[$class]['uniques'];
    }

    /**
     *
     * @return Type[]
     */
    public static function getColumns() {
        self::define();
        $class = static::getClass();
        return isset(self::$columns[$class]) ? self::$columns[$class] : [];
    }

    /**
     *
     * @return Type[]
     */
    public static function getColumnsNames() {
        self::define();

        $class = static::getClass();
        return isset(self::$columns[$class]) ? array_keys(self::$columns[$class]) : [];
    }

    /**
     *
     * @return Type[]
     */
    public static function getColumnsNamesAutoSelect() {
        self::define();

        $class = static::getClass();

        if (!isset(self::$columns[$class])) {
            return [];
        }
        $result = [];

        foreach (self::$columns[$class] as $name => $column) {
            if ($column->getAutoselect()) {
                $result[] = $name;
            }
        }

        return $result;
    }

    public static function getAutoCRUDConfig() {
        $columns = self::getColumns();
        foreach ($columns as $name => $column) {
            $result[] = ['name' => $name, 'config' => $column->getAutoCRUD()];
        }

        return $result;
    }

    /**
     *
     * @param string $name
     * @return Type
     */
    public static function isColumn($name) {
        $columns = self::getColumns();
        return isset($columns[$name]);
    }

    /**
     *
     * @param string $name
     * @return Type
     */
    public static function getColumn($name) {
        $columns = self::getColumns();
        if (!isset($columns[$name])) {
            $class = static::getClass();
            throw new Exception\ColumnDoesNotExist($name, $class);
        }

        return $columns[$name];
    }

    protected function init() {

    }

    protected function autoinit($mode) {

    }

    /**
     * @return Models
     */
    public static function find($field, $value, array $fields = array()) {
        return self::q(static::getConnection())
                        ->where($field, $value)
                        ->select($fields);
    }

    /**
     * @return Models
     */
    public static function all() {
        return self::q(static::getConnection())->select();
    }

    /**
     * @return static
     */
    public static function findOne($field, $value, $fk = self::FK_LOAD_NO_FK) {
        $tmp = self::q(static::getConnection())
                ->where($field, $value)
                ->one([], $fk);

        if (!isset($tmp)) {
            return false;
        }

        return $tmp;
    }

    /**
     * @return static
     */
    public static function countBy($field, $value) {
        return self::q(static::getConnection())
                        ->where($field, $value)
                        ->count();
    }

    /**
     * @return static
     */
    public static function findOneExcept($field, $value, $exceptArray, $fk = self::FK_LOAD_NO_FK) {
        if (empty($exceptArray[1])) {
            return false;
        }

        $sql = self::sql()
                ->where("{$field}", $value)
                ->andWhere("!{$exceptArray[0]}", $exceptArray[1]);

        $tmp = $sql->one();
        $tmp->loadForeignKeysData($fk);

        if (!isset($tmp)) {
            return false;
        }

        return $tmp;
    }

    final public static function name($column = null) {
        $class = static::getClass();
        $n = array_reverse(explode('\\', $class))[0];

        return $n . (($column !== null) ? '.' . $column : '');
    }

    final public static function n($column = null) {
        return static::name($column);
    }

    /**
     *
     * @param string $connection
     * @return \SORM\Sorm
     */
    final public static function sql(string $connection = null) {
        $class = static::getClass();
        $columns = static::getColumns();
        $table = static::getTableName();

        if (is_null($connection)) {
            $connection = static::getConnection();
        }

        $config = \SORM\Sorm::getConnection($connection);
        $db = Factory\Driver::newInstance($config);

        return new Sorm($db, $table, $class, null, $columns);
    }

    /**
     *
     * @param string $connection
     * @return \SORM\Query
     */
    final public static function q(string $connection = null, $alias = null) {
        $table = static::getTableName();
        $columns = static::getColumns();

        if (is_null($connection)) {
            $connection = static::getConnection();
        }
        if (!is_null($alias)) {
            $table .= " {$alias}";
        }

        return new Query($table, $connection, $columns);
    }

    /**
     *
     * @return boolean
     */
    public function isValid() {
        return true;
    }

    /**
     *  @return static
     */
    public function save($withForeign = false) {
        if (isset($this->id) && $this->id > 0) {
            $this->update($withForeign);
        } else {
            $this->insert($withForeign);
        }

        return $this;
    }

    /**
     *  @return static
     */
    public function saveLater($withForeign = false) {
        if (isset($this->id) && $this->id > 0) {
            $this->updateLater($withForeign);
        } else {
            $this->insertLater($withForeign);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function delete() {
        if (isset($this->id) && $this->id > 0) {
            $q = self::q(self::getConnection())
                    ->where('id', $this->id);
            return $q->delete();
        }

        return false;
    }

    public function deleteLater() {
        if (isset($this->id) && $this->id > 0) {
            $q = self::q(self::getConnection())
                    ->where('id', $this->id);
            return $q->deleteLater();
        }

        return false;
    }

    /**
     *
     * @param boolean $withForeign
     */
    final private function update($withForeign = false) {
        if (!$this->saving) {
            $this->saving = true;

            if ($withForeign && isset($this::$foreign)) {
                /* @todo Dodać obsługę foriegnKeys */
            }

            $data = [];

            foreach (array_keys($this->changes) as $name) {
                $data[$name] = $this->record[$name];
            }

            if ($data) {
                if ($this->isColumn('update_at')) {
                    $this->update_at = date(DATE_ATOM);
                }

                self::q(self::getConnection())
                        ->where('id', $this->id)
                        ->update($data);
                $this->changes = [];
            }
            $this->saving = false;
        }
    }

    /**
     *
     * @param boolean $withForeign
     */
    final private function updateLater($withForeign = false) {
        if (!$this->saving) {
            $this->saving = true;

            if ($withForeign && isset($this::$foreign)) {
                /* @todo Dodać obsługę foriegnKeys */
            }

            $data = [];

            foreach (array_keys($this->changes) as $name) {
                $data[$name] = $this->record[$name];
            }

            if ($data) {
                if ($this->isColumn('update_at')) {
                    $this->update_at = date(DATE_ATOM);
                }
                self::q(self::getConnection())
                        ->where('id', $this->id)
                        ->updateLater($data);
                $this->changes = [];
            }
            $this->saving = false;
        }
    }

    final public function getUpdateSql() {
        return self::sql()->getUpdateSql($this->record);
    }

    final public function getInsertSql() {
        return self::sql()->getInsertSql($this->record);
    }

    final public function getSql() {
        $sql = '';
        if (isset($this->id) && $this->id > 0) {
            $sql = self::q(self::getConnection())
                    ->where('id', $this->id)
                    ->makeSql(Query::QUERY_TYPE_UPDATE, $this->record);
        } else {
            $sql = self::q(self::getConnection())
                    ->makeSql(Query::QUERY_TYPE_INSERT, $this->record);
        }

        return $sql;
    }

    /**
     *
     * @param boolean $withForeign
     */
    final private function insert($withForeign = false) {
        if (!$this->saving) {

            $this->id = self::q(self::getConnection())
                    ->insert($this->record);

            self::$records[get_class($this)][$this->id] = &$this->record;

            $this->saving = true;

            if ($withForeign && isset($this::$foreign)) {
                /* @todo Dodać obsługę foriegnKeys */
            }

            $this->saving = false;
        }
    }

    /**
     *
     * @param boolean $withForeign
     */
    final private function insertLater($withForeign = false) {
        if (!$this->saving) {

            $this->id = self::q(self::getConnection())
                    ->insertLater($this->record);

            self::$records[get_class($this)][$this->id] = &$this->record;

            $this->saving = true;

            if ($withForeign && isset($this::$foreign)) {
                /* @todo Dodać obsługę foriegnKeys */
            }

            $this->saving = false;
        }
    }

    /**
     *
     * @return string
     */
    final private static function getClass() {
        return get_called_class();
    }

    final public function loadIfEmpty() {
        if (isset($this->id) && $this->id > 0) {
            if (!isset(self::$records[get_class($this)][$this->id]) || empty(self::$records[get_class($this)][$this->id])) {
                $tmp = self::sql()->where($this->id)->select();
                if (isset($tmp[0])) {
                    self::$records[get_class($this)][$this->id] = $tmp[0]->asArray();
                }
            }

            $this->record = &self::$records[get_class($this)][$this->id];

            if (isset($this::$foreign) && isset($this::$foreign[FK::ManyToOne])) {
                foreach ($this::$foreign[FK::ManyToOne] as $name => $par) {
                    if (!isset($this->record[$par['name']]) && isset($this->record[$name])) {
                        $rfc = new ReflectionClass($par['type']);
                        $this->record[$par['name']] = $rfc->newInstanceArgs(array('empty', $this->record[$name]));
                    }
                }
            }

            $this->empty = false;
        }
    }

    final public function __construct($mode = 'manual', $id = null, $fn = null) {
        $class = get_class($this);

        $this->createMode = $mode;
        $this->define();
        if ($mode == 'sorm') {

            if (empty($this->record)) {

                if (!isset(self::$records[$class][$this->id])) {
                    self::$records[$class][$this->id] = array();
                }
                if (!isset(self::$virtuals[$class][$this->id])) {
                    self::$virtuals[$class][$this->id] = array();
                }
                if (!isset(self::$virtuals[$class][$this->id])) {
                    self::$extras[$class][$this->id] = array();
                }

                $this->empty = false;
                $this->record = &self::$records[$class][$this->id];
                $this->virtual = &self::$virtuals[$class][$this->id];
            }

            foreach ($this->record as $column => $value) {
                if ($this->isColumn($column)) {
                    $this->getColumn($column)->finalNormalizeValue($value);
                    $this->record[$column] = $value;
                }
            }

            foreach ($this->getExtras() as $name => $extra) {
                $this->{$name} = $extra($this);
            }

            if ($fn instanceof \Closure) {
                $fn($this);
            }

            $this->autoinit($mode);
        } elseif ($mode == 'empty') {
            if (isset($id)) {
                $this->id = $id;

                if (isset(self::$records[$class][$this->id])) {
                    $this->empty = false;
                    $this->record = &self::$records[$class][$this->id];
                    $this->virtual = &self::$virtuals[$class][$this->id];
                }
            }
            $this->autoinit($mode);
        } else {
            $this->init();
        }
        $this->preinit = false;
    }

    public function __set($name, $value) {
        if ($name == 'id') {
            if ($this->preinit === false) {
                throw new \Exception('gdzieś ktoś ustawia id, a to przecież nie możliwe!');
            }
            $this->id = $value;
        }


        if ($this->isColumn($name)) {
            if ($this->preinit === true) {
                foreach ($this->getColumn($name)->getModifiers() as $modifier) {
                    $value = $modifier($value, $this);
                }
            } elseif (!isset($this->record[$name]) || $this->record[$name] !== $value || is_object($value) || is_array($value)) {
                if (!isset($this->changes[$name])) {
                    $this->changes[$name] = new Change($name, isset($this->record[$name]) ? $this->record[$name] : null, $value);
                } else {
                    $this->changes[$name]->setValue($value);
                }
            }

            $this->record[$name] = $value;
        } else {
            if ($this->isCalculated($name) && $this->preinit === true) {
                foreach ($this->getCalculated($name)->getModifiers() as $modifier) {
                    $value = $modifier($value, $this);
                }
            }
            $this->extra[$name] = $value;
        }
    }

    public function __wakeup() {
        if ((int) $this->id > 0 && !isset(self::$records[get_class($this)][$this->id])) {
            self::$records[get_class($this)][$this->id] = &$this->record;
        }
    }

    public function loadForeignKeysData($kind = self::FK_ALL) {
        foreach ($this->getForeignKeys($kind) as $name => $foreignKey) {
            $this->loadForeignKeyData($name);
        }
        return $this;
    }

    public function loadForeignKeyData($name) {
        $fk = $this->getForeignKey($name);
        if ($fk instanceof \SORM\Constraint\ForeignKey\OneToMany) {
            /** @var \SORM\Constraint\ForeignKey\OneToMany $fk */
            $m = get_class($this);
            $c = $fk->getColumn()->getName();
            $v = $fk->getColumn()->getValue($this);

            try {
                $this->virtual[$name] = \Engine5\Cache\Core::instance()->get('fk', $m . $c . $v);
            } catch (\Engine5\Cache\CacheExpireException $exc) {
                $this->virtual[$name] = $this
                        ->getForeignKey($name)
                        ->loadData($this);

                \Engine5\Cache\Core::instance()->set('fk', $m . $c . $v, $this->virtual[$name]);
            }
        } else {
            $this->virtual[$name] = $this
                    ->getForeignKey($name)
                    ->loadData($this);
        }

        return $this->virtual[$name];
    }

    public function __get($name) {
        if ($name == 'id') {
            return $this->id;
        }
        $sources = array_merge($this->record, $this->virtual, $this->extra);

        if (array_key_exists($name, $sources)) {
            return $sources[$name];
        } elseif ($this->isForeignKeys($name)) {
            return $this->loadForeignKeyData($name);
        } elseif ($this->isCalculated($name)) {
            return null;
        } else {
            $column = $this->getColumn($name);
            return $column->defaultValue();
        }
    }

    /**
     *
     * @return array
     */
    public final function jsonSerialize() {
        return $this->asArray();
    }

    /**
     *
     * @return array
     */
    public final function asArray($deep = true) {
        $items = array_merge(
                ['id' => $this->id], $this->record, $this->virtual, $this->extra
        );

        if (!$deep) {
            return $items;
        }

        $return = array();
        foreach ($items as $key => $item) {
            if (is_object($item) && method_exists($item, 'asArray')) {
                $return[$key] = $item->asArray();
            } elseif (is_object($item) && !method_exists($item, 'asArray')) {
                $return[$key] = (array) $item;
            } else {
                $return[$key] = $item;
            }
        }
        return $return;
    }

    public function __isset($name) {
        return $this->offsetExists($name);
    }

    public function __unset($name) {
        unset($this->record[$name], $this->virtual[$name], $this->extra[$name]);
    }

    /**
     *
     * @param string $name
     * @return boolean
     */
    public function offsetExists($name) {
        if ($name == 'id') {
            return true;
        }

        $sources = array_merge($this->record, $this->virtual, $this->extra);

        if (array_key_exists($name, $sources)) {
            return true;
        } elseif ($this->isForeignKeys($name)) {
            $this->loadForeignKeyData($name);
            return true;
        }

        return false;
//
//
//        if ($name === 'category') {
//            pd('offsetExists', $name, $this);
//        }
//        return (
//                $name == 'id' ||
//                isset($this->record[$name]) || isset($this->virtual[$name]) || isset($this->extra[$name])
//                );
    }

    public function offsetGet($name) {
        return $this->__get($name);
    }

    public function offsetSet($name, $value) {
        $this->__set($name, $value);
    }

    public function offsetUnset($name) {
        if ((int) $this->id <= 0 && $this->record[$name]) {
            unset($this->record[$name]);
        } elseif ((int) $this->id > 0 && isset(self::$records[get_class($this)][$this->id][$name])) {
            unset(self::$records[get_class($this)][$this->id][$name]);
        }
    }

    /**
     * @param array $data
     * @return static
     */
    public static function create(array $data = array(), $conection = null) {
        $class = static::getClass();
        $rfc = new \ReflectionClass($class);
        $_this = $rfc->newInstance();
        foreach ($data as $name => $value) {
            if ($name != 'id') {
                $_this->{$name} = $value;
            }
        }

        if ($conection) {
            $_this->setConnection($conection);
        }

        return $_this;
    }

    /**
     * @param array $data
     * @return static
     */
    public function loadFromRest($data) {
        foreach ($data as $name => $value) {
            if ($name != 'id' && $this->isColumn($name)) {
                @$this->{$name} = $value;
            }
        }
        return $this;
    }

    /**
     * @param array $data
     * @return static
     */
    public function setData(array $data) {
        $this->record = $data;
        return $this;
    }

    public static function setConnection($conection) {
        self::__conection($conection);
    }

    public static function getConnection() {
        self::define();
        $class = static::getClass();
        if (!isset(self::$connections[$class])) {
            self::$connections[$class] = 'default';
        }
        return self::$connections[$class];
    }

    public static function getTableName() {
        self::define();
        $class = static::getClass();
        if (!isset(self::$tables[$class])) {
            self::$tables[$class] = strtolower(preg_replace('/([A-Z])/', '_${1}', lcfirst(array_reverse(explode('\\', $class))[0])));
        }
        return self::$tables[$class];
    }

    public function getCreateSQL($replace = false) {
        $config = \SORM\Sorm::getConnection(self::getConnection());
        $db = \SORM\Factory\Driver::newInstance($config);
        /* @var $db Interfaces\Driver */
        $q = $db::FIELD_NAME_DELIMITER;

        $class = static::getClass();
        $table = static::getTableName();

        $_table = /* $replace ? $table . '_e5rpl' : */ $table;

        if (!$replace) {
            $sql = "CREATE TABLE {$q}{$_table}{$q} (\n";

            $sqls = array();

            foreach (self::$columns[$class] as $column) {
                /* @var $column Type */
                $sqls[] = "\t" . $column->getCreateSQL() . (($column->getName() == 'id') ? ' PRIMARY KEY' : '');
            }
            $constrSql = '';

            foreach (self::$constraints[$class] as $type => $list) {
                $_type = strtoupper($type);
                if ($list) {
                    $constraints = array();
                    foreach ($list as $constraint /* @var $constraint Constraint  */) {
                        $s = $constraint->getCreateSQL();
                        if (strlen($s) > 0) {
                            $constraints[] = $s;
                        }
                    }
                    if (count($constraints) > 0) {
                        $n = count($constraints);
                        $constrSql .= ",\n\n\t--{$_type}({$n})\n\t" . implode(",\n\t", $constraints);
                    }
                }
            }

            $sql .= implode(",\n", $sqls) . $constrSql . "\n);";
            foreach (self::$columns[$class] as $column) {
                $sql .= "\nCOMMENT ON COLUMN {$q}{$_table}{$q}.{$q}{$column->getName()}{$q} IS '" . json_encode($column->getCommentSQL()) . "';";
            }
        } else {
            $sql = "";
            $modelColumns = $this->getColumns();
            $tableColumns = $db->getColumns($table);

            $existsCols = array();
            $notExistsCols = array();
            foreach (array_keys($modelColumns) as $name) {
                if (isset($tableColumns[$name])) {
                    $existsCols[] = $name;
                } else {
                    $notExistsCols[] = $name;
                    $sql .= "\nALTER TABLE {$q}{$_table}{$q} ADD COLUMN " . $modelColumns[$name]->getCreateSQL() . ";";
                    $sql .= "\nCOMMENT ON COLUMN {$q}{$_table}{$q}.{$q}{$name}{$q} IS '" . json_encode($modelColumns[$name]->getCommentSQL()) . "';";
                }
            }
//
//            $colsStr = implode('", "', $existsCols);
//
//            $sql .= "\n\nSET CONSTRAINTS ALL DEFERRED;";
//            $sql .= "\n\nINSERT INTO \"{$_table}\"(\"{$colsStr}\")\nSELECT \"{$colsStr}\" FROM \"{$table}\";";
//            $sql .= "\n\nDROP TABLE \"{$table}\";";
//            $sql .= "\n\nALTER TABLE \"{$_table}\" RENAME TO \"{$table}\";";
//            $sql .= "\n\nSET CONSTRAINTS ALL IMMEDIATE;";
        }
        return $sql;
    }

}
