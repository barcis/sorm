<?php

namespace SORM;

/**
 * Description of Config
 *
 * @author barcis
 * @property-read $type
 * @property-read $host
 * @property-read $name
 * @property-read $user
 * @property-read $pass
 * @property-read $port
 * @property-read $prefix
 * @property-read $models
 * @property-read $uniqe
 */
class Config {

    /**
     *
     * @var array
     */
    private $config;

    public function __construct(array $cfg) {
        $this->config = $cfg;
    }

    public function __isset($name) {
        return isset($this->config[$name]);
    }

    public function __get($name) {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }
    }

    protected function set($name, $value) {
        $this->config[$name] = $value;
    }

    public function setConnectionName($value) {
        $this->config['uniqe'] = $value;
    }

    public function getUniqueId() {
        return md5($this->uniqe . $this->type . $this->host . $this->name . $this->user . $this->pass . $this->port . $this->prefix);
    }

}
