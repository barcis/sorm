<?php

/**
 *
 */

namespace SORM\Config\Parser;

/**
 * Description of Yaml
 *
 * @author barcis
 *
 */
class Yaml extends \SORM\Pattern\Getter implements \SORM\Interfaces\App\Config {

    public function __construct($filename, $field = null, $subfield = null) {
        $config = \SORM\Tools\Yaml::parseFile($filename);

        if (!empty($subfield)) {
            parent::__construct($config[$field][$subfield]);
        } elseif (!empty($field)) {
            parent::__construct($config[$field]);
        } else {
            parent::__construct($config);
        }
    }

}
