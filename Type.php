<?php

namespace SORM;

class Type implements \JsonSerializable {

    private $model = '';
    private $name = '';
    private $null = true;
    private $default = null;
    private $unique = false;
    private $sortType = 'alfa';
    private $autoselect = true;
    private $encrypted = false;
    private $comment = [];

    /**
     *
     * @var Constraint\ForeignKey\OneToMany[]
     */
    private $foreignKeys = [];

    /**
     *
     * @var Constraint\Check[]
     */
    private $checks = [];
    private $sqltype = '';
    private $modifiers = [];

    public function getName() {
        return $this->name;
    }

    public function getValue(Model $model) {
        return $model->{$this->name};
    }

    public function getModelName() {
        return $this->model;
    }

    public function getTableName() {
        return call_user_func(array($this->model, 'getTableName'));
    }

    public function getUnique() {
        return $this->unique;
    }

    public function canByNull() {
        return !$this->null;
    }

    /**
     *
     * @param boolean $value
     * @return static
     */
    public function setNull() {
        $this->null = true;
        return $this;
    }

    /**
     *
     * @param boolean $value
     * @return static
     */
    public function setNotNull() {
        $this->null = false;
        return $this;
    }

    /**
     *
     * @param mixed $value
     * @return static
     */
    public function setDefaultValue($value) {
        $this->default = $value;
        return $this;
    }

    /**
     *
     * @param boolean $value
     * @return static
     */
    public function isUnique($value) {
        $this->unique = $value;
        return $this;
    }

    /**
     *
     * @param boolean $value
     * @return static
     */
    public function setAutoselect($value) {
        $this->autoselect = $value;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getAutoselect() {
        return $this->autoselect;
    }

    /**
     *
     * @param boolean $value
     * @return static
     */
    public function setEncrypted($value) {
        $this->encrypted = $value;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getEncrypted() {
        return $this->encrypted;
    }

    /**
     *
     * @param string $refTableOrClass
     * @param string $refColumn
     * @param string $name
     * @return \SORM\Type
     * @throws \Exception
     * @todo Zmienić \Exception na jakiś bardziej odpowiedni rodzaj wyjątku
     */
    public function addForeignKeyOneToMany(string $field = null, string $refTableOrClass = null, string $refColumn = null, string $name = null) {
        if (is_null($refTableOrClass) || is_null($refColumn)) {
            $regexp = '/^(?<table>[A-Za-z0-9]+)\_(?<column>[A-Za-z0-9]+)$/';

            if (preg_match($regexp, $this->getName(), $matches)) {
                if (is_null($refTableOrClass)) {
                    $refTableOrClass = $matches['table'];
                }
                if (is_null($refColumn)) {
                    $refColumn = $matches['column'];
                }
            } else {
                throw new \Exception("nie da rady odgadnąć nazw tabel i kolumny dla ForeignKeyOneToMany z nazwy kolumny {$this->getName()}");
            }
        }

        $refClass = class_exists($refTableOrClass) ? $refTableOrClass : \SORM\Sorm::findModelByTableName($refTableOrClass);

        if (!$refClass) {
            throw new \Exception("Bad tablename or bad modelname! Model for table or model '{$refTableOrClass}' not exists ");
        }

        $model = (new \ReflectionClass($refClass))->newInstance();
        /* @var Model $model */

        if (is_null($field)) {
            $field = $model->getTableName();
        }

        $fk = new Constraint\ForeignKey\OneToMany($field, $this, $name);
        $fk->setReferenceModel($model);
        $fk->setReferenceColumn($model->getColumn($refColumn));

        $this->foreignKeys[$fk->getName()] = $fk;

        return $this;
    }

    /**
     *
     * @return static
     */
    public function addCheck(string $expresion, string $name = null) {
        $ck = new Constraint\Check($this, $name);
        $ck->setExpression($expresion);

        $this->checks[$ck->getName()] = $ck;

        return $this;
    }

    /**
     *
     * @return static
     */
    public function addModifier(callable $modifier) {
        $this->modifiers[] = $modifier;
        return $this;
    }

    /**
     *
     * @return Constraint\ForeignKey\OneToMany[]
     */
    public function getForeignKeysOneToMany() {
        return $this->foreignKeys;
    }

    /**
     *
     * @return Constraint\Check
     */
    public function getSortType() {
        return $this->sortType;
    }

    /**
     *
     * @return Constraint\Check
     */
    public function getChecks() {
        return $this->checks;
    }

    /**
     *
     * @return callable
     */
    public function getModifiers() {
        return $this->modifiers;
    }

    public function defaultValue() {
        return $this->default;
    }

    protected function __construct(string $name, string $sqltype, $sortType = 'alfa') {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
        $model = ($backtrace[2]['class'] === 'SORM\Model') ? $backtrace[3]['class'] : $backtrace[2]['class'];

        if (!is_subclass_of($model, 'SORM\Model') && $model != 'SORM\Model') {
            throw new \Exception(get_called_class() . " called out of 'SORM\Model'. Called from '{$model}!'");
        }

        $this->name = $name;
        $this->class = get_class($this);
        $this->sqltype = $sqltype;
        $this->type = $this->getSqlType();
        $this->model = $model;
        $this->sortType = $sortType;

        $this->comment = (object) [
                    'model' => $model . '::' . $name,
        ];
    }

    public function setComment($comment) {
        $this->comment->note = $comment;
    }

    public function getComment() {
        return isset($this->comment->note) ? $this->comment->note : '';
    }

    /**
     *
     * @return static
     */
    public function setAutoCRUD(AutoCRUD $AutoCRUD) {
        $this->comment->autocrud = $AutoCRUD;

        return $this;
    }

    public function getAutoCRUD() {
        if (!isset($this->comment->autocrud)) {
            $this->comment->autocrud = new AutoCRUD();
        }

        return $this->comment->autocrud;
    }

    public function getCommentSQL() {
        return $this->comment;
    }

    protected function getCreateSQLPartName(Driver $db) {
        $quote = $db::FIELD_NAME_DELIMITER;
        return "{$quote}{$this->getName()}{$quote}";
    }

    protected function getCreateSQLPartType(Driver $db) {
        return "{$this->getSqlType()}";
    }

    protected function getCreateSQLPartNull(Driver $db) {
        return $this->canByNull() ? "NOT NULL" : "NULL";
    }

    protected function getCreateSQLPartDefault(Driver $db) {
        if ($this->defaultValue() !== null) {
            $squote = $db::STRING_DELIMITER;
            return "DEFAULT {$squote}{$this->defaultValue()}{$squote}";
        }
        return "";
    }

    public function getCreateSQL() {
        $config = \SORM\Sorm::getConnection('default');
        $db = \SORM\Factory\Driver::newInstance($config);
        return "{$this->getCreateSQLPartName($db)} {$this->getCreateSQLPartType($db)} {$this->getCreateSQLPartNull($db)} {$this->getCreateSQLPartDefault($db)}";
    }

    protected function getSqlType() {
        if ($this->getName() === 'id') {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);

            return $db->getSqlIdType();
        }

        return $this->sqltype;
    }

    public function normalizeValue(&$value) {

    }

    public final function finalNormalizeValue(&$value) {
        if ($this->encrypted) {
            $decoded = base64_decode($value);
            $value = utf8_encode($decoded);
        }
        $this->normalizeValue($value);
    }

    public function getSqlName() {
        $config = \SORM\Sorm::getConnection('default');
        $db = \SORM\Factory\Driver::newInstance($config);

        return $this->getCreateSQLPartName($db);
    }

    public function getSqlValue($value) {
        return $value;
    }

    public final function getFinalSqlValue($value) {
        if (is_null($value)) {
            return 'NULL';
        }

        if ($this->encrypted) {
            $value = base64_encode($value);
        }

        $value = $this->getSqlValue($value);

        return $value;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

}
