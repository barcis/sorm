<?php

namespace SORM;

class StoredProcedure {

    /**
     *
     * @var Interfaces\Driver
     */
    private $db;
    private $result_type = 'ROWS';

    private function __construct($connection = 'default', $result_type = 'ROWS') {
        $config = \SORM\Sorm::getConnection($connection);
        $this->db = Factory\Driver::newInstance($config);
        $this->result_type = $result_type;
    }

    public static function q($connection = 'default') {
        return new StoredProcedure($connection, 'StoredProcedureResult');
    }

    public static function row($connection = 'default') {
        return new StoredProcedure($connection, 'ROW');
    }

    public static function rows($connection = 'default') {
        return new StoredProcedure($connection, 'ROWS');
    }

    public function __call($name, $arguments) {
        foreach ($arguments as &$argument) {
            $argument = new Query\Value($argument);
        }

        $args = implode(", ", $arguments);

        if ($this->result_type === 'ROWS') {
            return $this->db->query("SELECT * FROM \"public\".\"{$name}\"({$args})");
        } elseif ($this->result_type === 'ROW') {
            $r = $this->db->query("SELECT * FROM \"public\".\"{$name}\"({$args})");

            $c = $r->count();

            if ($c > 1) {
                throw new \Exception("Zwrócono wiele rekordów zamiast jednego!");
            } elseif ($c === 0) {
                return null;
            } else {
                return $r[0];
            }
        } elseif ($this->result_type === 'StoredProcedureResult') {
            $r = $this->db->query("SELECT * FROM \"public\".\"{$name}\"({$args})", '\SORM\StoredProcedureResult');

            $c = $r->count();
            if ($c > 1) {
                throw new \Exception("Zwrócono wiele rekordów ({$c}) zamiast jednego!");
            } elseif ($c === 0) {
                return null;
            } else {
                return $r[0];
            }
        }
    }

}
