<?php

namespace SORM\Exception;

/**
 * Description of ModelUnique
 *
 * @author barcis
 */
class BadFieldNameFormat extends \Exception {

    /**
     *
     * @var string
     */
    private $field;

    public function __construct($field) {
        $this->field = $field;

        parent::__construct("Field name '{$field}' has bad format! Available formats: [table|alias.]field [[as] alias] or instance of '\\SORM\\Query\\Raw'\n", 0, null);
    }

    /**
     *
     * @return string
     */
    public function getField() {
        return $this->field;
    }

}
