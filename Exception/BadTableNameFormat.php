<?php

namespace SORM\Exception;

/**
 * Description of ModelUnique
 *
 * @author barcis
 */
class BadTableNameFormat extends \Exception {

    /**
     *
     * @var string
     */
    private $table;

    public function __construct($table) {
        $this->table = $table;

        parent::__construct("Table name '{$table}' has bad format! Available formats: [table|alias.]field [[as] alias] or instance of '\\SORM\\Query\\Raw'\n", 0, null);
    }

    /**
     *
     * @return string
     */
    public function getField() {
        return $this->table;
    }

}
