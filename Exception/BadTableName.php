<?php

namespace SORM\Exception;

/**
 * Description of ModelUnique
 *
 * @author barcis
 */
class BadTableName extends \Exception {

    /**
     *
     * @var string
     */
    private $table;

    public function __construct($table) {
        $this->table = $table;

        parent::__construct("Bad table name '{$table}'! No model for this table.", 0, null);
    }

    /**
     *
     * @return string
     */
    public function getField() {
        return $this->table;
    }

}
