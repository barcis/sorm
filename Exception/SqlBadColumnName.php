<?php

namespace SORM\Exception;

/**
 * Description of SqlBadColumnName
 *
 * @author barcis
 */
class SqlBadColumnName extends \Exception implements \JsonSerializable {

    /**
     *
     * @var string
     */
    private $column;
    private $sql;
    private $pgerror;
    private $details;

    public function __construct($code, $column, $sql, $pgerror, $details) {
        $text = "BŁĘDNA NAZWA KOLUMNY!<br />\n";
        $text .= "Błędne zapytanie: <pre>{$sql}</pre><br />\n";
        $text .= "Treść błędu: " . $pgerror . "<br />\n";
        $text .= "Kod błędu: {$code}<br />\n";


        $this->column = $column;
        $this->sql = $sql;
        $this->pgerror = $pgerror;
        $this->details = $details;

        parent::__construct($text, $code, null);
    }

    /**
     *
     * @return string
     */
    public function getColumn() {
        return $this->column;
    }

    /**
     *
     * @return string
     */
    public function getSql() {
        return $this->sql;
    }

    /**
     *
     * @return string
     */
    public function getPgerror() {
        return $this->pgerror;
    }

    /**
     *
     * @return string[]
     */
    public function getDetails() {
        return $this->details;
    }

    public function jsonSerialize() {
        return [
            'code' => $this->getCode(),
            'sql' => $this->getSql(),
            'pgerror' => $this->getPgerror(),
            'column' => $this->getColumn(),
            'message' => $this->getMessage(),
            'details' => $this->getDetails()
        ];
    }

}
