<?php

namespace SORM\Exception;

/**
 * Description of ModelUnique
 *
 * @author barcis
 */
class ModelUnique extends \Exception {

    private $column;
    private $model;
    private $class;
    private $value;
    private $crash;

    public final function getColumn() {
        return $this->column;
    }

    public final function getModel() {
        return $this->model;
    }

    public final function getClass() {
        return $this->class;
    }

    public final function getValue() {
        return $this->value;
    }

    public final function getCrash() {
        return $this->crash;
    }

    public final function getCrashId() {
        return $this->crash->id;
    }

    public function __construct($column, \SORM\Model &$model, \SORM\Model &$crash) {
        $this->column = $column;
        $this->model = $model;
        $this->class = get_class($model);
        $this->value = $model->{ $column };
        $this->crash = $crash;

        $message = "Pole [{$column}] obiektu [{$this->class}] powinno być unikalna, ale wartość [{$this->value}] istnieje już w bazie w obiekcie o id [{$crash->id}]!";
        parent::__construct($message);
    }

}
