<?php

namespace SORM\Exception;

/**
 * Description of ModelUnique
 *
 * @author barcis
 */
class ColumnDoesNotExist extends \Exception {

    /**
     *
     * @var string
     */
    private $model;

    /**
     *
     * @var string
     */
    private $column;

    public function __construct($column, $model) {
        $this->column = $column;
        $this->model = $model;

        parent::__construct("Column '{$column}' does not exist in model '{$model}'");
    }

    /**
     *
     * @return string
     */
    public function getColumn() {
        return $this->column;
    }

    /**
     *
     * @return string
     */
    public function getModel() {
        return $this->model;
    }

}
