<?php

namespace SORM\Exception;

/**
 * Description of SqlUnique
 *
 * @author barcis
 */
class SqlUnique extends \Exception {

    private $details;

    public function __construct($message, $code, $details) {
        parent::__construct($message, $code);

        $this->details = $details;
    }

    public function getDetails() {
        return $this->details;
    }

}
