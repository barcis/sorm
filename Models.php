<?php

namespace SORM;

/**
 * Description of Models
 *
 * @author barcis
 */
class Models extends \SORM\Pattern\Lists {

    private $sql;
    private $elemType;
    private $deleted = array();

    public function getDeleted() {
        return $this->deleted;
    }

    public function offsetUnset($offset) {
        $this->deleted[] = $this->items[$offset];
        unset($this->items[$offset]);
    }

    public function __construct($sql = '', $elemType = '\SORM\Model') {
        $this->sql = $sql;
        $this->elemType = $elemType;
    }

    public function getSql() {
        return $this->sql;
    }

    public function getElementsType() {
        return $this->elemType;
    }

    public function getAsHtmlOptions($key, $value) {
        $options = array();
        foreach ($this->items as $item) {
            $options[$item->{$key}] = $item->{$value};
        }
        return $options;
    }

    private function flat(array &$options, array $array, $deep = 0, $parent = '') {
        foreach ($array as $m) {
            if (isset($m['childs'])) {
                $this->flat($options, $m['childs'], $deep + 1, $m['item']['value']);
            } else {
                $options[$m['item']['key']] = $parent . ($parent ? ' / ' : '') . $m['item']['value'];
            }
        }
    }

    public function getTreeAsHtmlOptions($treeKey, $key, $value) {
        $tmp = array();

        foreach ($this->items as $item) {

            $pid = (int) $item->{$treeKey};
            if (!isset($tmp[$item->id]['item'])) {
                $tmp[$item->id]['item'] = array(
                    'key' => $item->{$key},
                    'value' => $item->{$value}
                );
            }

            if (!isset($tmp[$pid])) {
                $tmp[$pid] = array('childs' => array());
            }

            if (!isset($tmp[$pid]['childs'])) {
                $tmp[$pid]['childs'] = array();
            }

            $tmp[$pid]['childs'][] = &$tmp[$item->id];
        }

        $options = array();

        $this->flat($options, $tmp[0]['childs'], 0);

        return $options;
    }

    public function union(Models $models) {
        foreach ($models as $model) {
            $this->append($model);
        }

        return $this;
    }

    public function uniqe($column = 'id') {
        $this->items = array_filter($this->items, function($obj) use ($column) {
            static $idList = array();
            if (in_array($obj->{$column}, $idList)) {
                return false;
            }
            $idList[] = $obj->{$column};
            return true;
        });
        return $this;
    }

    /**
     *
     * @param string $field
     * @param string $value
     * @return Models
     */
    public function filter($field, $value) {
        $not = substr($field, 0, 1) === '!';
        if ($not) {
            $field = substr($field, 1);
        }

        $ml = new Models('', $this->elemType);

        foreach ($this->items as $item) {
            if (is_array($value)) {
                if ($not && !in_array($item->{$field}, $value)) {
                    $ml[] = $item;
                } elseif (!$not && in_array($item->{$field}, $value)) {
                    $ml[] = $item;
                }
            } else {
                if ($not && $item->{$field} != $value) {
                    $ml[] = $item;
                } elseif (!$not && $item->{$field} == $value) {
                    $ml[] = $item;
                }
            }
        }
        return $ml;
    }

    private static function build_sorter_alfa($key, $sign = 1) {
        return function ($a, $b) use ($key, $sign) {
            setlocale(LC_ALL, 'pl-PL.utf8', 'pl_PL.UTF8', 'pl_PL.utf8', 'pl_PL.UTF-8', 'pl_PL.utf-8', 'polish_POLISH.UTF8', 'polish_POLISH.utf8', 'pl.UTF8', 'polish.UTF8', 'polish-pl.UTF8', 'PL.UTF8', 'polish.utf8', 'polish-pl.utf8', 'PL.utf8');
            return $sign * strcoll($a->{$key}, $b->{$key});
        };
    }

    private static function build_sorter_numeric($key, $sign = 1) {
        return function ($a, $b) use ($key, $sign) {
            if ((float) $a->{$key} == (float) $b->{$key}) {
                return 0;
            }

            return ($sign) * ((float) $a->{$key} > (float) $b->{$key} ? 1 : -1);
        };
    }

    public function orderby($column, $direction = 'ASC') {
        $direction = ($direction == 'ASC') ? 1 : -1;

        $i = $this->items;
        $it = reset($i);
        if ($it) {
            if ($it && !$it->isColumn($column) || $it->getColumn($column)->getSortType() == 'alfa') {
                usort($i, self::build_sorter_alfa($column, $direction));
            } else {
                usort($i, self::build_sorter_numeric($column, $direction));
            }
        }

        $ml = new Models('', $this->elemType);
        foreach ($i as $e) {
            $ml[] = $e;
        }

        return $ml;
    }

    /**
     *
     * @param string $field
     * @param string $value
     * @return Models
     */
    public function where($field, $value) {
        return $this->filter($field, $value);
    }

    /**
     *
     * @param type $field
     * @param type $value
     * @return Models
     */
    public function one($field, $value) {
        foreach ($this->items as $item) {
            if ($item->{$field} == $value) {
                return $item;
            }
        }
        return null;
    }

    /**
     *
     * @param type $field
     * @param type $value
     * @return Models
     */
    public function delete($field, $value) {
        $i = 0;
        foreach ($this->items as $key => $item) {
            if ($item->{$field} == $value) {
                $i++;
                unset($this->items[$key]);
            }
        }
        return $i;
    }

    /**
     *
     * @param int $field
     * @return Models
     */
    public function limit($limit) {
        $ml = new Models('', $this->elemType);
        foreach (array_slice($this->items, 0, $limit) as $item) {
            $ml[] = $item;
        }
        return $ml;
    }

    /**
     *
     * @param int $field
     * @return Models
     */
    public function offset($offset) {
        $ml = new Models('', $this->elemType);
        foreach (array_slice($this->items, $offset) as $item) {
            $ml[] = $item;
        }
        return $ml;
    }

    /**
     *
     * @param int $field
     * @return Models
     */
    public function slice($offset, $limit) {
        $ml = new Models('', $this->elemType);
        foreach (array_slice($this->items, $offset, $limit) as $item) {
            $ml[] = $item;
        }
        return $ml;
    }

}
