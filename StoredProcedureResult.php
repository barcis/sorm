<?php

namespace SORM;

/**
 * @property boolean $status
 * @property int $code
 * @property string $message
 */
class StoredProcedureResult {

    final public function __construct($mode = 'manual', $id = null, $fn = null) {

    }

    final public function asArray() {
        return (array) $this;
    }

}
