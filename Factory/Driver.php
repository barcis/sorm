<?php

namespace SORM\Factory;

/**
 * Description of Driver
 *
 * @author barcis
 */
class Driver {

    private static $instances = array();

    /**
     * @return \SORM\Interfaces\Driver
     */
    public static function newInstance(\SORM\Config $config) {
        if (!isset(self::$instances[$config->uniqe])) {
            $rfc = new \ReflectionClass($config->type);
            self::$instances[$config->uniqe] = $rfc->newInstance($config);
        }
        return self::$instances[$config->uniqe];
    }

}
