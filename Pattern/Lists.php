<?php

namespace SORM\Pattern;

/**
 * Description of EP_list
 *
 * @author barcis
 */
class Lists implements \ArrayAccess, \Iterator, \JsonSerializable, \Countable {

    /**
     * @var array
     */
    protected $items = array();
    private $position = 0;

    public function insert($key, $value, $position) {
        $this->items = array_merge(array_slice($this->items, 0, $position), array($key => $value), array_slice($this->items, $position));
    }

    public function append($value) {
        $this->items[] = $value;
    }

    public function offsetExists($offset) {
        return isset($this->items[$offset]);
    }

    public function offsetGet($offset) {
        if ($offset === ':first') {
            return $this->first();
        } elseif ($offset === ':last') {
            return $this->last();
        }

        if (!isset($this->items[$offset])) {
            return null;
        }

        return $this->items[$offset];
    }

    public function offsetSet($offset, $value) {
        if (!isset($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset) {
        unset($this->items[$offset]);
    }

    public function count() {
        return count($this->items);
    }

    public function isEmpty() {
        return ($this->count() === 0);
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        $tmp = array_keys($this->items);
        if (isset($tmp[$this->position]) && isset($this->items[$tmp[$this->position]])) {
            return $this->items[$tmp[$this->position]];
        }
        return null;
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        $this->position++;
    }

    public function valid() {
        $tmp = array_keys($this->items);
        return isset($tmp[$this->position]);
    }

    public function getItems() {
        return $this->items;
    }

    public function first() {
        return reset($this->items);
    }

    public function last() {
        return end($this->items);
    }

    /**
     *
     * @return array
     */
    public final function jsonSerialize() {
        return $this->asArray();
    }

    public final function reverse() {
        $this->items = array_reverse($this->items);
        return $this;
    }

    public function asArray($deep = true) {
        if (!$deep) {
            return $this->items;
        }

        $return = array();
        foreach ($this->items as $item) {
            if (is_object($item) && method_exists($item, 'asArray')) {
                $return[] = $item->asArray();
            } elseif (is_object($item) && !method_exists($item, 'asArray')) {
                $return[] = (array) $item;
            } else {
                $return[] = $item;
            }
        }
        return $return;
    }

}
