<?php

namespace SORM;

class AutoCRUD {

    public $editable = true;
    public $type = null;
    /**/
    public $formShow = true;
    public $formLabel = null;
    public $formOrder = 9999;
    /**/
    public $listShow = true;
    public $listLabel = null;
    public $listOrder = 9999;

    /**
     *
     * @param string $label
     * @return \SORM\AutoCRUD
     */
    public function setLabel($label) {
        return $this->setListLabel($label)->setFormLabel($label);
    }

    /**
     *
     * @param string $label
     * @return \SORM\AutoCRUD
     */
    public function setFormLabel($label) {
        $this->formLabel = $label;
        return $this;
    }

    /**
     *
     * @param string $label
     * @return \SORM\AutoCRUD
     */
    public function setListLabel($label) {
        $this->listLabel = $label;
        return $this;
    }

    /**
     *
     * @param int $order
     * @return \SORM\AutoCRUD
     */
    public function setOrder($order) {
        return $this->setListOrder($order)->setFormOrder($order);
    }

    /**
     *
     * @param int $order
     * @return \SORM\AutoCRUD
     */
    public function setFormOrder($order) {
        $this->formOrder = $order;
        return $this;
    }

    /**
     *
     * @param int $order
     * @return \SORM\AutoCRUD
     */
    public function setListOrder($order) {
        $this->listOrder = $order;
        return $this;
    }

    /**
     *
     * @param boolean $editable
     * @return \SORM\AutoCRUD
     */
    public function setEditable($editable) {
        $this->editable = $editable;
        return $this;
    }

    /**
     *
     * @param string $type
     * @return \SORM\AutoCRUD
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @param boolean $show
     * @return \SORM\AutoCRUD
     */
    public function setShow($show) {
        return $this->setListShow($show)->setFormShow($show);
    }

    /**
     *
     * @param boolean $show
     * @return \SORM\AutoCRUD
     */
    public function setFormShow($show) {
        $this->listShow = $show;
        return $this;
    }

    /**
     *
     * @param boolean $show
     * @return \SORM\AutoCRUD
     */
    public function setListShow($show) {
        $this->listShow = $show;
        return $this;
    }

    public function __construct(array $setup = []) {
        foreach ($setup as $key => $value) {
            $this->{$key} = $value;
        }
    }

}
