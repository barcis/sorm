<?php

namespace SORM\Interfaces;

interface Driver {

    function __construct(\SORM\Config $config);

    function debug($debug);

    function exec($sql);

    function test();

    function query($sql, $targetClass, $autocreate = true);

    function count($sql);

    function delete($sql);

    function translateDbType($type);

    function getColumns($table);
}
