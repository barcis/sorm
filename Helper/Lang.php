<?php

namespace SORM\Helper;

trait Lang {

    private static $defaultLanguage = 'pl';

    public static function langAutoModifier($value) {

        $newValue = is_object($value) ? $value : json_decode($value, false);

        if (is_null($newValue)) {
            $newValue = (object) [
                        'hasLangs' => true,
                        self::$defaultLanguage => $value
            ];
        } elseif (!isset($newValue->hasLangs)) {
            $newValue->hasLangs = true;
        }

        if (!isset($newValue->{self::$defaultLanguage})) {
            $newValue->{self::$defaultLanguage} = '';
        }

        return $newValue;
    }

}
