<?php

namespace SORM\Helper;

trait JSON {

    private static function columnsListToJSON(array $columns, $stop = false) {
        return '\'[\'||array_to_string(array_agg(' . self::columnListToJSON($columns, $stop) . '), \',\')||\']\'';
    }

    private static function distinctColumnsListToJSON(array $columns, $stop = false) {
        return '\'[\'||array_to_string(array_agg(distinct(' . self::columnListToJSON($columns, $stop) . ')), \',\')||\']\'';
    }

    private static function columnListToJSON(array $columns, $stop = false) {

        $cols = [];

        foreach ($columns as $key => $column) {
            $c = new \SORM\Query\Field($column);
            $c->setCoalesce(new \SORM\Query\Value("null"), 'VARCHAR');
            $c->setConcatSurround('"');

            if (is_numeric($key)) {
                $key = $c->getField()->getName();
            }

//            $cols[] = '"' . $key . '":\'||coalesce(\'"\'||' . $c . '::varchar||\'"\',\'null\')||\'';
            $cols[] = '"' . $key . '":\'||' . ($c) . '||\'';
        }
        if ($stop) {
            dd($cols);
        }

        return "'{" . implode(',', $cols) . "}'";
    }

}
