<?php

namespace SORM;

use Composer\Script\Event;

/**
 * Description of Sorm
 *
 * @author barcis
 */
class Commands {

    private static function init(Event $event) {
        define('ST_VENDOR', realpath(__DIR__ . '/../../'));
        define('APPSDIR', realpath(__DIR__ . '/../../../apps/'));
        define('SQLSDIR', realpath(APPSDIR . '/../sql/'));

        $args = $event->getArguments();

        if (!isset($args[0])) {
            echo "Musisz podać nazwę aplikacji dla której ma być nawiązane połączenie!\n\n";
            return;
        }

        $AppName = $args[0];
        $AppDir = APPSDIR . '/content/' . $AppName . '/';

        if (!file_exists($AppDir)) {
            echo "Podana aplikacja '{$AppName}' nie istnieje!\n\n";
            return;
        }

        $AppDBConfigFile = $AppDir . 'config/database.config.yml';

        if (!file_exists($AppDBConfigFile)) {
            echo "Podana aplikacja '{$AppName}' nie ma pliku konfiguracyjnego bazy danych!\n\n";
            return;
        }


        \Engine5\Core\Engine::initialize();

        $dbConfigs = \Engine5\Tools\Yaml::parseFile($AppDBConfigFile);

        if (isset($args[1])) {
            $dbDefault = $args[1];
        } else {
            $dbDefault = $dbConfigs['default'];
        }

//        if (!isset($dbConfigs['databases'][$dbDefault])) {
//            echo "Podana podana nazwa połączenia '{$dbDefault}' nie istnieje!\n\n";
//            return;
//        }

        $dbConfig = $dbConfigs['databases'][$dbDefault];

        \SORM\Sorm::setDefaultConnection(new \SORM\Config($dbConfig));

        $config = \SORM\Sorm::getConnection();
        return Factory\Driver::newInstance($config);
    }

    public static function getProcedures(Event $event) {
        $db = self::init($event);

        if (!$db) {
            return;
        }

        $procedures = $db->query('
            SELECT  p.proname AS name
            FROM    pg_catalog.pg_namespace n
            JOIN    pg_catalog.pg_proc p ON (p.pronamespace = n.oid)
            WHERE   n.nspname = ANY (current_schemas(false))
            ', 'stdClass', false);

        $dir = SQLSDIR . '/procedures/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        $dir = realpath($dir);

        foreach ($procedures as $procedure) {
            $prc = $db->query("
                SELECT n.nspname as \"schema\",
                       p.proname as \"name\",
                       pg_catalog.pg_get_function_result(p.oid) AS \"result\",
                       pg_catalog.pg_get_function_arguments(p.oid) as \"arguments\",
                       CASE
                        WHEN p.proisagg THEN 'agg'
                        WHEN p.proiswindow THEN 'window'
                        WHEN p.prorettype = 'pg_catalog.trigger'::pg_catalog.regtype THEN 'trigger' ELSE 'normal'
                       END as \"type\",
                       CASE WHEN prosecdef THEN 'definer' ELSE 'invoker' END AS \"security\",
                       CASE
                        WHEN p.provolatile = 'i' THEN 'immutable'
                        WHEN p.provolatile = 's' THEN 'stable'
                        WHEN p.provolatile = 'v' THEN 'volatile'
                       END as \"volatility\",
                       pg_catalog.pg_get_userbyid(p.proowner) as \"owner\",
                       l.lanname as \"language\",
                       p.prosrc as \"code\"
                FROM   pg_catalog.pg_proc p
                 LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                 LEFT JOIN pg_catalog.pg_language l ON l.oid = p.prolang
                WHERE  p.proname = '{$procedure->name}'
                AND    pg_catalog . pg_function_is_visible(p.oid);
                ")[0];

            $data = "CREATE OR REPLACE FUNCTION \"{$prc->name}\"({$prc->arguments})\n" .
                    "  RETURNS {$prc->result} AS\n\$BODY\$\n{$prc->code}\n\$BODY\$\n" .
                    "  LANGUAGE plpgsql VOLATILE\n" .
                    "  COST 100;";

            echo "\033[0mSaving Procedure \033[32m{$prc->name}\033[0m\n";
            file_put_contents($dir . '/' . $prc->name . '.sql', $data);
        }
    }

    public static function getViews(Event $event) {
        $db = self::init($event);

        if (!$db) {
            return;
        }

        $views = $db->query('
            SELECT "table_name" AS "vname", \'V\' as "vtype" FROM "information_schema"."views" WHERE "table_schema" = ANY (current_schemas(false))
            UNION
            SELECT "matviewname" AS "vname", \'M\' as "vtype" FROM "pg_matviews" WHERE "schemaname" = ANY (current_schemas(false))');

        $dir = SQLSDIR . '/views/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        $dir = realpath($dir);

        foreach ($views as $view) {
            $def = $db->query("select pg_get_viewdef('{$view->vname}', true) as definition;
")[0]->definition;

            $data = "CREATE" . ($view->vtype === 'M' ? " MATERIALIZED" : "") . " VIEW {$view->vname} AS \n" . $def;

            echo "\033[0mSaving View \033[32m{$view->vname}\033[0m\n";
            file_put_contents($dir . '/' . $view->vname . '.sql', $data);
        }
    }

}
