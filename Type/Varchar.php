<?php

namespace SORM\Type;

/**
 * Description of Varchar
 *
 * @author barcis
 */
class Varchar extends \SORM\Type {

    private $size = 256;

    public function __construct($name) {
        parent::__construct($name, 'VARCHAR');
    }

    /**
     *
     * @param integer $size
     * @return static
     */
    public function setSize($size) {
        $this->size = $size;
        return $this;
    }

    protected function getSqlType() {
        return parent::getSqlType() . "({$this->size})";
    }

    public function getSqlValue($value) {
        return "'" . pg_escape_string($value) . "'";
    }

}
