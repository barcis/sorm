<?php

namespace SORM\Type;

/**
 * Description of Boolean
 *
 * @author barcis
 */
class Boolean extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'BOOLEAN');
    }

    protected function getCreateSQLPartDefault(\SORM\Driver $db) {
        if ($this->defaultValue() !== null) {
            if ($this->defaultValue() === true) {
                $d = 'TRUE';
            } elseif ($this->defaultValue() === false) {
                $d = 'FALSE';
            } else {
                $d = $this->defaultValue();
            }

            return "DEFAULT {$d}";
        }
        return "";
    }

    public function getSqlValue($value) {
        return $value ? 'true' : 'false';
    }

}
