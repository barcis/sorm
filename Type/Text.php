<?php

namespace SORM\Type;

/**
 * Description of Text
 *
 * @author barcis
 */
class Text extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'TEXT');
    }

    public function getSqlValue($value) {
        if (!is_string($value)) {
            $value = json_encode($value);
        }
        return "'" . pg_escape_string($value) . "'";
    }

}
