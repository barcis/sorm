<?php

namespace SORM\Type;

/**
 * Description of Time
 *
 * @author barcis
 */
class Time extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'time');
    }

    public function getSqlValue($value) {
        return "'" . $value . "'";
    }

}
