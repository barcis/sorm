<?php

namespace SORM\Type;

/**
 * Description of Numeric
 *
 * @author barcis
 */
class Numeric extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'numeric(10,2)', 'numeric');
    }

    public function normalizeValue(&$value) {
        parent::normalizeValue($value);
        $value = (float) $value;
    }

    public function getSqlValue($value) {
        $value = (float) $value;
        $value = parent::getSqlValue($value);
        return $value;
    }

}
