<?php

namespace SORM\Type;

/**
 * Description of Inet
 *
 * @author barcis
 */
class Inet extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'INET', 'alfa');
    }

    public function getSqlValue($value) {
        return "'" . pg_escape_string($value) . "'";
    }

}
