<?php

namespace SORM\Type;

/**
 * Description of Date
 *
 * @author barcis
 */
class Date extends \SORM\Type {

    protected function getCreateSQLPartDefault(\SORM\Driver $db) {
        if ($this->defaultValue() !== null) {
            if ($this->defaultValue() === "'now'::timestamp" || $this->defaultValue() === "'now'" || $this->defaultValue() === "now") {
                return "DEFAULT (now())::timestamp without time zone";
            } else {
                $squote = $db::STRING_DELIMITER;
                return "DEFAULT {$squote}{$this->defaultValue()}{$squote}";
            }
        }
        return "";
    }

    public function __construct($name) {
        parent::__construct($name, 'date');
    }

    public function getSqlValue($value) {
        return "'" . $value . "'";
    }

}
