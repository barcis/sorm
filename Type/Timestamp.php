<?php

namespace SORM\Type;

/**
 * Description of Timestamp
 *
 * @author barcis
 */
class Timestamp extends \SORM\Type {

    public function __construct($name, $withTimeZone = true) {
        if ($withTimeZone) {
            parent::__construct($name, 'timestamp with time zone');
        } else {
            parent::__construct($name, 'timestamp without time zone');
        }
    }

    protected function getCreateSQLPartDefault(\SORM\Driver $db) {
        if ($this->defaultValue() !== null) {
            return "DEFAULT {$this->defaultValue()}";
        }
        return "";
    }

    public function getSqlValue($value) {
        return "'" . $value . "'";
    }

}
