<?php

namespace SORM\Type;

/**
 * Description of Number
 *
 * @author barcis
 */
class Number extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'FLOAT', 'numeric');
    }

    public function normalizeValue(&$value) {
        parent::normalizeValue($value);
        $value = (float) $value;
    }

    public function getSqlValue($value) {
        $value = (float) $value;
        $value = parent::getSqlValue($value);
        return $value;
    }

}
