<?php

namespace SORM\Type;

/**
 * Description of Json
 *
 * @author barcis
 */
class Json extends \SORM\Type {

    public function __construct($name) {

        $con = \SORM\Sorm::getConnection();
        $var = isset($con->version) ? $con->version : '8.4';

        parent::__construct($name, version_compare($var, '9.2.0') >= 0 ? 'JSONB' : 'TEXT');
    }

    public function normalizeValue(&$value) {
        parent::normalizeValue($value);
        try {
            if (empty($value)) {
                $value = null;
            } elseif (!is_object($value) && !is_array($value)) {
                $newValue = @json_decode(@json_decode($value));
                $value = (!is_null($newValue)) ? $newValue : @json_decode($value);

                if ($value === 'null') {
                    $value = null;
                }
            }
        } catch (Exception $ex) {
            $value = null;
        }
    }

    public function getSqlValue($value) {
        $value = "'" . pg_escape_string(json_encode($value)) . "'";

        return $value;
    }

}
