<?php

namespace SORM\Type;

/**
 * Description of Integer
 *
 * @author barcis
 */
class Integer extends \SORM\Type {

    public function __construct($name) {
        parent::__construct($name, 'INTEGER', 'numeric');
    }

    public function normalizeValue(&$value) {
        parent::normalizeValue($value);
        $value = (int) $value;
    }

    public function getSqlValue($value) {
        $value = (int) $value;
        $value = parent::getSqlValue($value);
        return $value;
    }

}
