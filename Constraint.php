<?php

namespace SORM;

abstract class Constraint {

    private $quote;

    public function __construct($name) {
        $this->name = $name;

        $config = \SORM\Sorm::getConnection('default');
        $db = \SORM\Factory\Driver::newInstance($config);
        /* @var $db Driver\Pgsql */
        $this->quote = $db::FIELD_NAME_DELIMITER;
    }

    public function getName() {
        return $this->name;
    }

    protected function getQuote() {
        return $this->quote;
    }

    protected function quote() {
        return $this->getQuote();
    }

    abstract function getCreateSQL();
}
