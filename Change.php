<?php

namespace SORM;

class Change {

    private $name;
    private $value;
    private $values = [];

    public function __construct($name, $oldvalue, $newvalue) {
        $debug = $this->getDebug(debug_backtrace(2));

        $this->name = $name;
        $this->values[] = ['value' => $oldvalue, 'changed' => $debug];
        $this->value = $newvalue;
    }

    public function setValue($newvalue) {
        $debug = $this->getDebug(debug_backtrace(2));

        $this->values[] = ['value' => $this->value, 'changed' => $debug];
        $this->value = $newvalue;
    }

    private function getDebug($d) {
        $debug = '';
        if (isset($d[1]['file'])) {
            $debug .= $d[1]['file'];
        }
        if (isset($d[1]['line'])) {
            $debug .= ':' . $d[1]['line'];
        }
        return $debug;
    }

}
