<?php

namespace SORM;

/**
 * Description of Query
 *
 * @author barcis
 *
 * @method \SORM\Query as($alias) Foo
 *
 * @method \SORM\Query where($where, $params) Foo
 * @method \SORM\Query whereIsNull($field) Foo
 * @method \SORM\Query whereIsNotNull($field) Foo
 * @method \SORM\Query whereIsTrue($field) Foo
 * @method \SORM\Query whereIsNotTrue($field) Foo
 * @method \SORM\Query whereIsFalse($field) Foo
 * @method \SORM\Query whereIsNotFalse($field) Foo
 * @method \SORM\Query whereIn($field, $items) Foo
 * @method \SORM\Query whereNotIn($field) Foo
 * @method \SORM\Query whereLike($field) Foo
 * @method \SORM\Query whereNotLike($field) Foo
 * @method \SORM\Query whereSimilar($field) Foo
 * @method \SORM\Query whereNotSimilar($field) Foo
 * @method \SORM\Query whereILike($field) Foo
 * @method \SORM\Query whereNotILike($field) Foo
 * @method \SORM\Query whereISimilar($field) Foo
 * @method \SORM\Query whereNotISimilar($field) Foo
 * @method \SORM\Query whereNot($field) Foo
 *
 * @method \SORM\Query andWhere($where, $sing = null, $params = null) Foo
 * @method \SORM\Query andWhereIsNull($field) Foo
 * @method \SORM\Query andWhereIsNotNull($field) Foo
 * @method \SORM\Query andWhereIsTrue($field) Foo
 * @method \SORM\Query andWhereIsNotTrue($field) Foo
 * @method \SORM\Query andWhereIsFalse($field) Foo
 * @method \SORM\Query andWhereIsNotFalse($field) Foo
 * @method \SORM\Query andWhereIn($field) Foo
 * @method \SORM\Query andWhereNotIn($field) Foo
 * @method \SORM\Query andWhereLike($field) Foo
 * @method \SORM\Query andWhereNotLike($field) Foo
 * @method \SORM\Query andWhereSimilar($field) Foo
 * @method \SORM\Query andWhereNotSimilar($field) Foo
 * @method \SORM\Query andWhereILike($field) Foo
 * @method \SORM\Query andWhereNotILike($field) Foo
 * @method \SORM\Query andWhereISimilar($field) Foo
 * @method \SORM\Query andWhereNotISimilar($field) Foo
 * @method \SORM\Query andWhereNot($field) Foo
 *
 * @method \SORM\Query orWhere($where, $sing = null, $params = null) Foo
 * @method \SORM\Query orWhereIsNull($field) Foo
 * @method \SORM\Query orWhereIsNotNull($field) Foo
 * @method \SORM\Query orWhereIsTrue($field) Foo
 * @method \SORM\Query orWhereIsNotTrue($field) Foo
 * @method \SORM\Query orWhereIsFalse($field) Foo
 * @method \SORM\Query orWhereIsNotFalse($field) Foo
 * @method \SORM\Query orWhereIn($field) Foo
 * @method \SORM\Query orWhereNotIn($field) Foo
 * @method \SORM\Query orWhereLike($field) Foo
 * @method \SORM\Query orWhereNotLike($field) Foo
 * @method \SORM\Query orWhereSimilar($field) Foo
 * @method \SORM\Query orWhereNotSimilar($field) Foo
 * @method \SORM\Query orWhereILike($field) Foo
 * @method \SORM\Query orWhereNotILike($field) Foo
 * @method \SORM\Query orWhereISimilar($field) Foo
 * @method \SORM\Query orWhereNotISimilar($field) Foo
 * @method \SORM\Query orWhereNot($field) Foo
 * */
class Query {

    /**
     *
     * @var Query\From[]|Query\Froms
     */
    private $tables;

    /**
     *
     * @var Query\From[]|Query\Froms
     */
    private $appendTables;

    /**
     *
     * @var Query\Join[]|Query\Joins
     */
    private $joins;

    /**
     *
     * @var Query\Field[]|Query\Fields
     */
    private $fields;

    /**
     *
     * @var Query\Where[]|Query\Wheres
     */
    private $wheres;

    /**
     *
     * @var Query\Orderby[]|Query\Orderbys
     */
    private $orderBys;

    /**
     *
     * @var Query\GroupBy[]|Query\GroupBys
     */
    private $groupBys;

    /**
     *
     * @var string
     */
    private $distinct;

    /**
     *
     * @var Query
     */
    private $supquery;

    /**
     *
     * @var string
     */
    private $alias;

    /**
     *
     * @var integer
     */
    private $limit;

    /**
     *
     * @var integer
     */
    private $offset;

    /**
     *
     * @var Interfaces\Driver
     */
    private $connection;
    private $db;
    private $columns = [];

    public function getWheres() {
        $wheres = clone $this->wheres;
        $wheres->setWithWhere(false);
        return $wheres;
    }

    public function __construct($table = null, $connection = 'default', $columns = null) {

        $this->tables = new Query\Froms();
        $this->appendTables = new Query\Froms(false);
        $this->joins = new Query\Joins();
        $this->fields = new Query\Fields();
        $this->wheres = new Query\Wheres();
        $this->wheres->setWithWhere(true);
        $this->groupBys = new Query\GroupBys();
        $this->orderBys = new Query\OrderBys();
        $this->columns = $columns;

        if (!is_null($table)) {
            $this->tables[$table] = new Query\From($table);
        }

        $this->connection = $connection;
        $config = \SORM\Sorm::getConnection($connection);
        $this->db = Factory\Driver::newInstance($config);
    }

    /**
     *
     * @param \SORM\Query\From $table
     * @return \SORM\Query
     */
    public function appendFrom($table) {
        if (is_array($table)) {
            foreach ($table as $key => $t) {
                $this->appendTables[$key] = ($t instanceof Query\From) ? $t : new Query\From($t, $key);
            }
        } else {
            if (($table instanceof Query)) {
                $name = $this->getFirstTableName()->getTable()->getName();
                $this->appendTables[$name] = $table;
            } elseif (($table instanceof Query\From)) {
                $this->appendTables[$table] = $table;
            } else {
                $this->appendTables[md5($table)] = new Query\From($table);
            }
        }

        return $this;
    }

    /**
     *
     * @param \SORM\Query\From $table
     * @return \SORM\Query
     */
    public function from($table) {
        if (is_array($table)) {
            foreach ($table as $key => $t) {
                $this->tables[$key] = ($t instanceof Query\From) ? $t : new Query\From($t, $key);
            }
        } else {
            if (($table instanceof Query)) {
                $name = $this->getFirstTableName()->getTable()->getName();
                $this->tables[$name] = $table;
            } elseif (($table instanceof Query\From)) {
                $this->tables[$table] = $table;
            } else {
                $this->tables[md5($table)] = new Query\From($table);
            }
        }

        return $this;
    }

    /**
     *
     * @param string|Query\From|Query\Join $table
     * @param \SORM\Query\Joiner|\SORM\Query\Joiners|Array $joiner
     * @param string $direction
     * @return \SORM\Query
     * @throws \InvalidArgumentException
     */
    public function join($table, $joiner, $direction = Query\Join::JOIN_INNER) {

        if ($table instanceof Query\Join) {
            $this->joins[] = $table;
        } elseif ($table instanceof Query\Raw) {
            $this->joins[] = new Query\Join($table);
        } else {
            $_table = ($table instanceof Query\From) ? $table : new Query\From($table);

            $this->joins[] = new Query\Join($_table, $joiner, $direction);
        }

        return $this;
    }

    public function leftJoin($table, $joiner) {
        return $this->join($table, $joiner, Query\Join::JOIN_LEFT);
    }

    public function rightJoin($table, $joiner) {
        return $this->join($table, $joiner, Query\Join::JOIN_RIGHT);
    }

    /**
     *
     * @param integer $limit
     * @return \SORM\Query
     */
    public function limit($limit) {
        $this->limit = $limit;

        return $this;
    }

    /**
     *
     * @param integer $offset
     * @return \SORM\Query
     */
    public function offset($offset) {
        $this->offset = $offset;

        return $this;
    }

    /**
     *
     * @param boolean $distinct
     * @return \SORM\Query
     */
    public function setDistinct($distinct) {
        $this->distinct = $distinct;
        return $this;
    }

    /**
     *
     * @param \SORM\Query $supquery
     * @return \SORM\Query
     */
    public function setSupquery(Query $supquery) {
        $this->supquery = $supquery;
        return $this;
    }

    /**
     *
     * @param string $alias
     * @return \SORM\Query
     */
    public function setAlias(string $alias) {
        $this->alias = new Query\Name($alias, Query\Name::NAME_TYPE_ALIAS);
        return $this;
    }

    const QUERY_TYPE_SELECT = 1;
    const QUERY_TYPE_COUNT = 2;
    const QUERY_TYPE_DELETE = 3;
    const QUERY_TYPE_INSERT = 4;
    const QUERY_TYPE_UPDATE = 5;

    public function getFirstTableName() {
        return $this->tables[':first'];
    }

    public function makeSql($queryType = self::QUERY_TYPE_SELECT, $values = []) {

        if ($queryType == self::QUERY_TYPE_SELECT) {
            if ($this->fields->isEmpty()) {
                $tab = $this->tables[':first'];
                $fields = $tab->getColumnsNamesAutoSelect();
                $this->addFields($fields, $tab->getTable());
            }

            $distinct = $this->distinct ? 'DISTINCT ' : '';

            if (is_object($this->limit)) {
                pd('$this->limit', $this->limit);
            }

            $sql = "    "
                    . "SELECT {$distinct}{$this->fields}\n"
                    . "{$this->tables}"
                    . "{$this->joins}"
                    . "{$this->appendTables}"
                    . "{$this->wheres}"
                    . "{$this->groupBys}"
                    . "{$this->orderBys}"
                    . (!is_null($this->limit) ? "     LIMIT {$this->limit}\n" : '')
                    . (!is_null($this->offset) ? "    OFFSET {$this->offset}\n" : '');
        } elseif ($queryType == self::QUERY_TYPE_COUNT) {

            $append = $this->groupBys->getString(false);
            if ($append) {
                $append = ', ' . $append;
            }

            $sql = "    "
                    . "SELECT count(1) AS \"count\"" . $append
                    . "{$this->tables}"
                    . "{$this->joins}"
                    . "{$this->appendTables}"
                    . "{$this->wheres}"
                    . "{$this->groupBys}";
        } elseif ($queryType == self::QUERY_TYPE_DELETE) {
            $tab = $this->tables[':first'];

            $sql = "    "
                    . "DELETE FROM {$tab->getTable()}"
                    . "{$this->wheres}";
        } elseif ($queryType == self::QUERY_TYPE_INSERT) {
            $tab = $this->tables[':first'];
            $fields = $tab->getColumnsNames();


            $sqlColumns = [];
            $sqlValues = [];

            foreach ($fields as $field) {
                if ($field === 'id' || !isset($values[$field])) {
                    continue;
                }
                $sqlColumns[] = $this->columns[$field]->getSqlName();
                $sqlValues[] = $this->columns[$field]->getFinalSqlValue($values[$field]);
            }
            $sql = '    '
                    . "INSERT INTO {$tab->getTable()} (";
            $sql .= implode(',', $sqlColumns);
            $sql .= ') VALUES (';
            $sql .= implode(',', $sqlValues);
            $sql .= ')';
        } elseif ($queryType == self::QUERY_TYPE_UPDATE) {
            $datas = array();
            $tab = $this->tables[':first'];
            $fields = $tab->getColumnsNames();
            foreach ($fields as $field) {
                if ($field === 'id' || !array_key_exists($field, $values)) {
                    continue;
                }
                $name = $this->columns[$field]->getSqlName();
                $value = $this->columns[$field]->getFinalSqlValue($values[$field]);

                $datas[] = "{$name}={$value}";
            }

            $_datas = implode(', ', $datas);

            $sql = "UPDATE {$this->tables[':first']} SET {$_datas} {$this->wheres}";
        } else {
            throw new \Exception('nie wiem jaki to typ: ' . $queryType);
        }

        return $sql;
    }

    public $calculateds = array();

    private function addField($field, $table = null) {
        if ($field instanceof Query) {
            $field = clone $field;
            $field->setSupquery($this);
        } elseif (is_string($field) && $field === '*') {
            $tab = $this->tables[':first'];
            $fields = $tab->getColumnsNames();
            $this->addFields($fields, $tab->getTable());
            return;
        } elseif (is_string($field) && $field[0] === '~') {
            $index = !is_null($table) ? $table : ':first';

            $alias = substr($field, 1);
            $calculated = $this->tables[$index]->getCalculated($alias);

            $this->calculateds[$alias] = $calculated;
            $field = $calculated->getField($this);
        } elseif (!($field instanceof Query\Field)) {
            $field = new Query\Field($field, $table);
        }
        $this->fields[] = $field;

        return $this;
    }

    public static function whereOperatorLeft($op, $left, $right) {
        $op = $name = strtoupper(preg_replace('/([a-z])([A-Z])/', '$1 $2', $op));
        return [$left, $op, null];
    }

    public static function whereOperatorCenter($op, $left, $right) {
        $op = strtoupper(preg_replace('/([a-z])([A-Z])/', '$1 $2', $op));
        return [$left, $op, $right];
    }

    public static $whereOperators = array(
        'IsNull' => [__CLASS__, 'whereOperatorLeft'],
        'IsNotNull' => [__CLASS__, 'whereOperatorLeft'],
        'IsTrue' => [__CLASS__, 'whereOperatorLeft'],
        'IsNotTrue' => [__CLASS__, 'whereOperatorLeft'],
        'IsFalse' => [__CLASS__, 'whereOperatorLeft'],
        'IsNotFalse' => [__CLASS__, 'whereOperatorLeft'],
        'In' => [__CLASS__, 'whereOperatorCenter'],
        'NotIn' => [__CLASS__, 'whereOperatorCenter'],
        'Like' => [__CLASS__, 'whereOperatorCenter'],
        'NotLike' => [__CLASS__, 'whereOperatorCenter'],
        'ILike' => [__CLASS__, 'whereOperatorCenter'],
        'NotILike' => [__CLASS__, 'whereOperatorCenter'],
        'Similar' => [__CLASS__, 'whereOperatorCenter'],
        'NotSimilar' => [__CLASS__, 'whereOperatorCenter'],
        'ISimilar' => [__CLASS__, 'whereOperatorCenter'],
        'NotISimilar' => [__CLASS__, 'whereOperatorCenter'],
        'Not' => [__CLASS__, 'whereOperatorCenter'],
    );

    public function __call($name, $arguments) {
        $whereOperatorsStr = implode("|", array_keys(static::$whereOperators));

        $regExp = "/^(?<operation>and|or)?(?<method>[Ww]here)(?<operator>{$whereOperatorsStr})?$/";

        if ($name == 'as') {
            $this->setAlias($arguments[0]);
        } elseif (preg_match($regExp, $name, $matches)) {
            $matches["method"] = strtolower($matches["method"]);

            if ($matches["method"] === 'where') {
                $operation = (isset($matches["operation"]) && !empty($matches["operation"])) ? $matches["operation"] : 'and';

                $where = isset($arguments[0]) ? $arguments[0] : null;
                $sing = isset($arguments[1]) ? $arguments[1] : null;
                $param = isset($arguments[2]) ? $arguments[2] : null;

                if (!isset($matches["operator"])) {
                    $this->addWhere($operation, $where, $sing, $param);
                } elseif (isset(static::$whereOperators[$matches["operator"]])) {
                    $opm = static::$whereOperators[$matches["operator"]];
                    $args = $opm($matches["operator"], $where, $sing, $param);
                    $this->addWhere($operation, $args[0], $args[1], $args[2]);
                } else {
                    throw new \BadMethodCallException($name);
                }
            }
        } else {
            throw new \BadMethodCallException($name);
        }

        return $this;
    }

    private function addWhere($operation, $where, $sing = null, $param = null) {
        if (!($where instanceof Query\Where)) {
            $where = new Query\Where($where, $sing, $param);
        }

        /* @var Query\Where $where */

        if (count($this->wheres) > 0) {
            $where->setOperation($operation);
        }

        $this->wheres[] = $where;

        return $this;
    }

    private function addFields(array $fields, $table = null) {
        foreach ($fields as $field) {
            $this->addField($field, $table);
        }

        return $this;
    }

    /**
     *
     * @param \SORM\Query\GroupBy|Array|string $field
     * @param string $direction DESC|ASC
     * @return \SORM\Query
     */
    public function groupBy($field) {
        if ($field instanceof Query\GroupBy) {
            $this->orderBys[] = $field;
        } elseif (is_array($field)) {
            foreach ($field as $d) {
                $this->orderBy($d);
            }
        } elseif (!is_null($field)) {
            $this->groupBys[] = new Query\GroupBy($field);
        }

        return $this;
    }

    /**
     *
     * @param \SORM\Query\OrderBy|Array|string $field
     * @param string $direction ASC|DESC
     * @param string $nulls FIRST|LAST
     * @return \SORM\Query
     */
    public function resetOrderBy() {
        $this->orderBys = new Query\OrderBys();

        return $this;
    }

    public function orderBy($field, $direction = 'ASC', $nulls = 'LAST') {
        if ($field instanceof Query\OrderBy) {
            $this->orderBys[] = $field;
        } elseif (is_array($field)) {
            foreach ($field as $f => $d) {
                if (!is_integer($f) && (is_null($d) || in_array(strtolower($d), array('asc', 'desc')))) {
                    $this->orderBy($f, $d);
                } else {
                    $this->orderBy($d);
                }
            }
        } elseif (!is_null($field)) {
            $this->orderBys[] = new Query\OrderBy($field, $direction, $nulls);
        }

        return $this;
    }

    /**
     *
     * @param array $fields
     * @return Models
     */
    public function fields(array $fields = array()) {
        $this->fields = new Query\Fields();
        $this->addFields($fields);

        return $this;
    }

    /**
     *
     * @param array $fields
     * @return Models
     */
    public function correlated($field1, $field2) {
        $this->where($field1, new Query\Field($field2));

        return $this;
    }

    /**
     *
     * @param array $fields
     * @return Models
     */
    public function select(array $fields = [], $cache = false) {
        $this->addFields($fields);
        $query = $this->makeSql(self::QUERY_TYPE_SELECT);

        if ($this->tables[':first'] instanceof Query) {
            $targetClass = $this->tables[':first']->getFirstTableName()->getModel();
        } else {
            $targetClass = $this->tables[':first']->getModel();
        }

        $cachekey = md5($query);

        if (!($cache === false)) {
            try {
                $return = \Engine5\Cache\Core::instance()->get('sql', $cachekey);
            } catch (\Engine5\Cache\CacheExpireException $exc) {
                $return = $this->db->query($query, $targetClass);
                \Engine5\Cache\Core::instance()->set('sql', $cachekey, $return);
            }
        } else {
            $return = $this->db->query($query, $targetClass);
        }

        return $return;
    }

    public function query($query) {
        $targetClass = $this->tables[':first']->getModel();

        return $this->db->query($query, $targetClass);
    }

    public function delete() {
        $query = $this->makeSql(self::QUERY_TYPE_DELETE);
        return $this->db->delete($query);
    }

    public function insert(array $fields) {
        $query = $this->makeSql(self::QUERY_TYPE_INSERT, $fields);
        return $this->db->insert($query);
    }

    public function update(array $fields) {
        $query = $this->makeSql(self::QUERY_TYPE_UPDATE, $fields);
        return $this->db->update($query);
    }

    public function deleteLater() {
        $query = $this->makeSql(self::QUERY_TYPE_DELETE);
        return $this->db->deleteLater($query);
    }

    public function insertLater(array $fields) {
        $query = $this->makeSql(self::QUERY_TYPE_INSERT, $fields);
        return $this->db->insertLater($query);
    }

    public function updateLater(array $fields) {
        $query = $this->makeSql(self::QUERY_TYPE_UPDATE, $fields);
        return $this->db->updateLater($query);
    }

    /**
     *
     * @param array $fields
     * @return Models
     */
    public function count($cache = false) {
        $query = $this->makeSql(self::QUERY_TYPE_COUNT);

        $cachekey = md5($query);

        if (!($cache === false)) {
            try {
                $return = \Engine5\Cache\Core::instance()->get('sql', $cachekey);
            } catch (\Engine5\Cache\CacheExpireException $exc) {

                if (count($this->groupBys) > 0) {
                    $return = $this->db->query($query);
                } else {
                    $return = (integer) $this->db->query($query)[0]->count;
                }

                \Engine5\Cache\Core::instance()->set('sql', $cachekey, $return);
            }
        } else {
            if (count($this->groupBys) > 0) {
                $return = $this->db->query($query);
            } else {
                $return = (integer) $this->db->query($query)[0]->count;
            }
        }

        return $return;
    }

    /**
     *
     * @param array $fields
     * @return Model
     */
    public function one(array $fields = array(), $fk = Model::FK_LOAD_NO_FK) {
        $this->limit(1);
        $data = $this->select($fields)[0];

        if (!isset($data)) {
            return false;
        }

        $data->loadForeignKeysData($fk);

        return $data;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function getSql($type = self::QUERY_TYPE_SELECT) {
        return $this->makeSql($type);
    }

    public function __toString() {
        $sql = $this->getSql();

        if ($this->supquery instanceof Query) {
            $sql = "($sql\n){$this->alias}";
        }
        return $sql;
    }

}
