<?php

namespace SORM\Constraint;

class Unique extends \SORM\Constraint {

    /**
     *
     * @var \SORM\Type
     */
    private $field;

    public function __construct(\SORM\Type $field, string $name = NULL) {

        $this->field = $field;
        if (empty($name)) {
            $name = $field->getTableName() . '_' . $this->field->getName() . '_uq';
        }

        parent::__construct($name);
    }

    public function getCreateSQL() {
        $q = $this->quote();
        //CONSTRAINT {$q}{$this->getName()}{$q}
        return "UNIQUE ({$q}{$this->field->getName()}{$q})";
    }

}
