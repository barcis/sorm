<?php

namespace SORM\Constraint\ForeignKey;

use SORM\Constraint\ForeignKey\ManyToMany\Joiner;

class ManyToMany extends \SORM\Constraint\ForeignKey {

    /**
     *
     * @var string
     */
    private $model;

    /**
     *
     * @var string
     */
    private $referenceModel;

    /**
     *
     * @var Joiner
     */
    private $joiner;
    private $modifiers = [];

    public function __construct(string $refTableOrClass, string $field = null) {

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $model = $backtrace[1]['class'];

        if (!is_subclass_of($model, 'SORM\Model')) {
            throw new \Exception(get_called_class() . " called out of 'SORM\Model'. Called from '{$model}!'");
        }


        $this->model = $model;
        $this->referenceModel = class_exists($refTableOrClass) ? $refTableOrClass : \SORM\Sorm::findModelByTableName($refTableOrClass);

        if (!$this->referenceModel) {
            //@todo: Better exception type
            throw new \Exception("Bad tablename or bad modelname! Model for table or model '{$refTableOrClass}' not exists ");
        }

        $joinerModel = $this->getJoinerModel($model, $this->referenceModel);

        $left = $this->getJoinerField($model);
        $right = $this->getJoinerField($this->referenceModel);

        $this->joiner = (new ManyToMany\Joiner($joinerModel))
                ->setLeft($left)
                ->setRight($right);

        if (is_null($field)) {
            $field = call_user_func([$this->referenceModel, 'getTableName']) . 's';
        }

        parent::__construct($field);
    }

    private function getJoinerField(string $model) {
        return call_user_func([$model, 'getTableName']) . '_id';
    }

    private function getJoinerModel(string $model, string $referenceModel) {
        $table = call_user_func([$model, 'getTableName']) . '_' . call_user_func([$referenceModel, 'getTableName']);
        $smodel = \SORM\Sorm::prepareModelNameFromTableName($table);

        $class = \SORM\Sorm::findModelByTableName($smodel);
        if (!$class) {
            $table = call_user_func([$referenceModel, 'getTableName']) . '_' . call_user_func([$model, 'getTableName']);
            $smodel = \SORM\Sorm::prepareModelNameFromTableName($table);

            $class = \SORM\Sorm::findModelByTableName($smodel);
            if (!$class) {
                throw new \SORM\Exception\BadTableName($table);
            }
        }

        return $class;
    }

    public function getCreateSQL() {
        return "";
    }

    private $queryModifier;

    public function getQuery(callable $modifier) {
        $this->queryModifier = $modifier;
        return $this;
    }

    public function addRowModifier(callable $modifier) {
        $this->modifiers[] = $modifier;
        return $this;
    }

    public function loadData(\SORM\Model $model) {
        $query = $this->joiner->query($model, $this->referenceModel);

        if (is_callable($this->queryModifier)) {
            $q = $this->queryModifier;
            $q($query);
        }

        $datas = $this->joiner->loadData();

        if ($this->modifiers) {
            $count = count($datas);
            for ($n = 0; $n < $count; $n++) {
                foreach ($this->modifiers as $modifier) {
                    $datas[$n] = $modifier($datas[$n]);
                }
            }
        }

        return $datas;
    }

}
