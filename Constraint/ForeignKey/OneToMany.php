<?php

namespace SORM\Constraint\ForeignKey;

class OneToMany extends \SORM\Constraint\ForeignKey {

    const ACTION_NO_ACTION = 'NO ACTION';
    const ACTION_RESTRICT = 'RESTRICT';
    const ACTION_CASCADE = 'CASCADE';
    const ACTION_SET_NULL = 'SET NULL';
    const ACTION_SET_DEFAULT = 'SET DEFAULT';
    const MATCH_FULL = 'FULL';
    const MATCH_PARTIAL = 'PARTIAL';
    const MATCH_SIMPLE = 'SIMPLE';

    /**
     *
     * @var \SORM\Model
     */
    private $referenceModel;
    private $column;
    private $match = self::MATCH_SIMPLE;
    private $onDelete = self::ACTION_NO_ACTION;
    private $onUpdate = self::ACTION_NO_ACTION;

    /**
     *
     * @var \SORM\Type
     */
    private $referenceColumn;

    public function __construct(string $field, \SORM\Type $column) {
        $this->column = $column;
        parent::__construct($field);
    }

    /**
     *
     * @return \SORM\Type
     */
    public function getColumn() {
        return $this->column;
    }

    /**
     *
     * @param \SORM\Model $referenceClass
     * @return static
     */
    public function setReferenceModel(\SORM\Model $referenceClass) {
        $this->referenceModel = $referenceClass;
        return $this;
    }

    /**
     *
     * @param \SORM\Type $referenceColumn
     * @return \SORM\Constraint\ForeignKey\OneToMany
     */
    public function setReferenceColumn(\SORM\Type $referenceColumn) {
        $this->referenceColumn = $referenceColumn;
        return $this;
    }

    public function setMatch($match) {
        if (!in_array($match, [self::MATCH_SIMPLE, self::MATCH_PARTIAL, self::MATCH_FULL])) {
            throw new Exception('bad value for match');
        }
        $this->match = $match;
        return $this;
    }

    public function setOnDelete($onDelete) {
        if (!in_array($onDelete, [self::ACTION_CASCADE, self::ACTION_NO_ACTION, self::ACTION_RESTRICT, self::ACTION_SET_DEFAULT, self::ACTION_SET_NULL])) {
            throw new Exception('bad value for onDelete');
        }
        $this->onDelete = $onDelete;
        return $this;
    }

    public function setOnUpdate($onUpdate) {
        if (!in_array($onUpdate, [self::ACTION_CASCADE, self::ACTION_NO_ACTION, self::ACTION_RESTRICT, self::ACTION_SET_DEFAULT, self::ACTION_SET_NULL])) {
            throw new Exception('bad value for onUpdate');
        }
        $this->onUpdate = $onUpdate;
        return $this;
    }

    public function getCreateSQL() {
        $q = $this->quote();

        return "FOREIGN KEY ({$q}{$this->column->getName()}{$q}) REFERENCES {$q}{$this->referenceModel->getTableName()}{$q} ({$q}{$this->referenceColumn->getName()}{$q}) " .
                "MATCH {$this->match}\n\t" .
                "ON UPDATE {$this->onUpdate}\n\t" .
                "ON DELETE {$this->onUpdate}\n\t" .
                "DEFERRABLE INITIALLY DEFERRED";
    }

    public function loadData(\SORM\Model $model) {
        $value = $this->getColumn()->getValue($model);
        $name = $this->referenceColumn->getName();

        return is_null($value) ? null : $this->referenceModel->findOne($name, $value);
    }

}
