<?php

namespace SORM\Constraint\ForeignKey\ManyToMany;

class Joiner {

    /**
     *
     * @var string
     */
    private $model;

    /**
     *
     * @var string
     */
    private $left;

    /**
     *
     * @var string
     */
    private $right;

    function __construct(string $model) {
        $this->model = $model;
    }

    /**
     *
     * @param string $model
     * @return \SORM\Constraint\ForeignKey\ManyToMany\Joiner
     */
    public function setModel(string $model) {
        $this->model = $model;
        return $this;
    }

    /**
     *
     * @param string $left
     * @return \SORM\Constraint\ForeignKey\ManyToMany\Joiner
     */
    public function setLeft(string $left) {
        $this->left = $left;
        return $this;
    }

    /**
     *
     * @param string $right
     * @return \SORM\Constraint\ForeignKey\ManyToMany\Joiner
     */
    public function setRight(string $right) {
        $this->right = $right;
        return $this;
    }

    /**
     *
     * @var \SORM\Query
     */
    private $query = null;

    public function query(\SORM\Model $model, string $target) {
        $table = call_user_func([$this->model, 'getTableName']);
        $targetTable = call_user_func([$target, 'getTableName']);

        $this->query = call_user_func([$target, 'q'])
                ->join($table, new \SORM\Query\Joiner($targetTable . '.id', $table . '.' . $this->right))
                ->where($this->left, $model->id);
        return $this->query;
    }

    public function loadData() {
        return $this->query->select();
    }

}
