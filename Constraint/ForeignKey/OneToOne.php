<?php

namespace SORM\Constraint\ForeignKey;

class OneToOne extends \SORM\Constraint\ForeignKey {

    /**
     *
     * @var string
     */
    private $model;
    private $orderBy = [];

    /**
     *
     * @var string
     */
    private $referenceModel;

    /**
     *
     * @var string
     */
    private $column = 'id';

    /**
     *
     * @var string
     */
    private $referenceColumn;

    public function __construct(string $refTableOrClass, string $field = null, string $refColumn = null, string $column = null) {

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $model = $backtrace[1]['class'];

        if (!is_subclass_of($model, 'SORM\Model')) {
            throw new \Exception(get_called_class() . " called out of 'SORM\Model'. Called from '{$model}!'");
        }

        if (!is_null($column)) {
            $this->column = $column;
        }

        $referenceModel = class_exists($refTableOrClass) ? $refTableOrClass : \SORM\Sorm::findModelByTableName($refTableOrClass);
        if (!$referenceModel) {
            throw new \Exception("Bad tablename or bad modelname! Model for table or model '{$refTableOrClass}' not exists ");
        }

        if (is_null($field)) {
            $field = \SORM\Sorm::prepareModelNameFromTableName(call_user_func([$referenceModel, 'getTableName']), false) . 's';
        }
        if (is_null($field)) {
            $field = \SORM\Sorm::prepareModelNameFromTableName(call_user_func([$referenceModel, 'getTableName']), false) . 's';
        }

        if (is_null($refColumn)) {
            $refColumn = call_user_func([$model, 'getTableName']) . '_id';
        }

        $this->referenceModel = $referenceModel;
        $this->referenceColumn = $refColumn;


        parent::__construct($field);
    }

    public function orderBy($field, $direction = 'ASC') {
        $this->orderBy[] = (object) ['field' => $field, 'direction' => $direction];
        return $this;
    }

    public function getCreateSQL() {
        return "";
    }

    public function loadData(\SORM\Model $model) {

        if (!isset($model->{$this->column}) || empty($model->{$this->column})) {
            return null;
        }

        $query = call_user_func([$this->referenceModel, 'q'])
                ->where($this->referenceColumn, $model->{$this->column});
        if ($this->wheres && count($this->wheres) > 0) {
            $query
                    ->where($this->wheres);
        }


        /* @var $query \SORM\Query */
        if (is_array($this->orderBy)) {
            foreach ($this->orderBy as $o) {
                $query->orderBy($o->field, $o->direction);
            }
        }

        return $query->one();
    }

}
