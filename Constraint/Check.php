<?php

namespace SORM\Constraint;

class Check extends \SORM\Constraint {

    /**
     *
     * @var \SORM\Type
     */
    private $field;

    /**
     *
     * @var string
     */
    private $expression;

    public function __construct(\SORM\Type $field, string $name = NULL) {

        $this->field = $field;
        if (empty($name)) {
            $name = $field->getTableName() . '_' . $this->field->getName() . '_ck';
        }

        parent::__construct($name);
    }

    public function setExpression(string $expression) {
        $this->expression = $expression;
    }

    public function getCreateSQL() {
        $q = $this->quote();
        //CONSTRAINT {$q}{$this->getName()}{$q}
        return "CHECK ({$q}{$this->field->getName()}{$q} {$this->expression})";
    }

}
