<?php

namespace SORM\Query;

/**
 * Description of OrderBy
 *
 * @author barcis
 */
class OrderBy {

    /**
     * string
     */
    private $model;

    /**
     * string
     */
    private $table;

    /**
     * string
     */
    private $field;

    /**
     * string
     */
    private $direction = 'ASC';

    /**
     * string
     */
    private $nulls = 'LAST';

    /**
     * Raw
     */
    private $raw;

    /**
     * string
     */
    static private $q;

    /**
     *
     * @var string
     */
    static private $regExpTableField = '/^((?<table>[a-zA-Z0-9\_]+)\.)?(?<field>[a-zA-Z0-9\_]+)(\ +(?<direction>([dD][eE]|[aA])[sS][cC]+))?(\ +[nN][uU][lL][lL][sS]\ +(?<nulls>([fF][iI][rR]|[lL][aA])[sS][tT]+))?$/';

    public function __construct($field, $direction = 'ASC', $nulls = 'LAST') {

        if ($field instanceof Raw) {
            $this->raw = $field;
            return;
        }

        if (preg_match(self::$regExpTableField, $field, $mathes)) {

            $_table = !empty($mathes['table']) ? $mathes['table'] : null;
            $this->field = $mathes['field'];
            $this->direction = strtoupper(!empty($mathes['direction']) ? $mathes['direction'] : $direction);
            $this->nulls = strtoupper(!empty($mathes['nulls']) ? $mathes['nulls'] : $nulls);

            if (!empty($_table)) {
                $this->model = \SORM\Sorm::findModelByClassName($_table);

                if (!$this->model) {
                    $this->model = \SORM\Sorm::findModelByTableName($_table);
                }

                $this->table = !$this->model ? $_table : call_user_func([$this->model, 'getTableName']);
            }
        } else {
            throw new \SORM\Exception\BadFieldNameFormat($field);
        }

        if (is_null(self::$q)) {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);
            /* @var $db Interfaces\Driver */
            self::$q = $db::FIELD_NAME_DELIMITER;
        }
        $q = self::$q;

        $this->raw = new Raw((!empty($this->table) ? "{$q}{$this->table}{$q}." : '' ) . "{$q}{$this->field}{$q} {$this->direction} NULLS {$this->nulls}");
    }

    public function getTable() {
        return $this->table;
    }

    public function getField() {
        return $this->field;
    }

    public function getDirection() {
        return $this->direction;
    }

    public function __toString() {
        return (string) $this->raw;
    }

}
