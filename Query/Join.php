<?php

namespace SORM\Query;

/**
 * Description of Join
 *
 * @author barcis
 */
class Join {

    /**
     *
     * @var From
     */
    private $table;

    /**
     *
     * @var string
     */
    private $tableName;

    /**
     *
     * @var Joiners
     */
    private $joiners;

    /**
     *
     * @var string
     */
    private $direction;

    /**
     *
     * @var Raw
     */
    private $raw;

    const JOIN_FULL = 'FULL OUTER';
    const JOIN_LEFT = 'LEFT OUTER';
    const JOIN_RIGHT = 'RIGHT OUTER';
    const JOIN_CROSS = 'CROSS';
    const JOIN_INNER = 'INNER';

    /**
     *
     * @param string|From|Join|Raw $table
     * @param Joiner|Joiners|Array $joiner
     * @param string $direction
     */
    public function __construct($table, $joiner, $direction = self::JOIN_INNER) {

        $this->joiners = new Joiners();

        if ($table instanceof Raw) {
            $this->raw = $table;
            return;
        }

        if ($joiner instanceof Joiner) {
            $this->joiners[] = $joiner;
        } elseif ($joiner instanceof Joiners) {
            $this->joiners = $joiner;
        } elseif (is_array($joiner)) {
            foreach ($joiner as $_joiner) {
                if (!($_joiner instanceof Joiner)) {
                    throw new \InvalidArgumentException('Bad argument $joiners');
                }
                $this->joiners[] = $_joiner;
            }
        } else {
            throw new \InvalidArgumentException('Bad argument $joiner');
        }

        $this->table = ($table instanceof From) ? $table : new From($table);
        $this->tableName = $this->table->getTable();
        $this->direction = ($direction instanceof JoinDirection) ? $direction : new JoinDirection($direction);

        $this->raw = new Raw($this->direction . ' JOIN ' . $this->table . "\n             ON " . $this->joiners);
    }

    public function __toString() {
        return (string) $this->raw;
    }

}
