<?php

namespace SORM\Query;

/**
 * Description of Field
 *
 * @author barcis
 */
class Field {

    /**
     * string
     */
    private $model;

    /**
     * Name
     */
    private $table;

    /**
     * Name
     */
    private $field;

    /**
     * Name
     */
    private $alias;

    /**
     * Raw
     */
    private $raw;

    /**
     * Array
     */
    private $defaults;

    /**
     * String
     */
    private $defaultsCast = null;

    /**
     * String
     */
    private $concatSurround = null;
    private $functions = [];

    /**
     * Array
     */
    private $casts;

    /**
     *
     * @var string
     */
    static private $regExpTableField = '/^((?<table>[a-zA-Z0-9\_]+)\.)?(?<field>([a-zA-Z0-9\_]+|\*))(?<json>([\-\#]\>{1,2}[a-z0-9]+)+)?(?<col>\[[0-9]+\])?((\ +[aA][sS])?\ +(?<alias>[a-zA-Z0-9\_]+))?$/';

    public function __construct($field, $table = null) {

        if ($field instanceof Raw) {
            $this->raw = $field;
            return;
        } elseif ($field instanceof \SORM\Query) {
            $this->raw = "$field";

            return;
        }

        if (preg_match(self::$regExpTableField, $field, $mathes)) {

            $_table = !empty($mathes['table']) ? $mathes['table'] : (($table instanceof Name) ? $table->getName() : $table);
            $this->field = ($mathes['field'] == '*' ? '*' : new Name($mathes['field'], Name::NAME_TYPE_FIELD, (isset($mathes['json']) ? $mathes['json'] : null), (isset($mathes['col']) ? $mathes['col'] : null)));
            $this->alias = new Name(!empty($mathes['alias']) ? $mathes['alias'] : '', Name::NAME_TYPE_ALIAS);

            if (!empty($_table)) {



                $this->model = \SORM\Sorm::findModelByClassName($_table);

                if (!$this->model) {
                    $this->model = \SORM\Sorm::findModelByTableName($_table);
                }

                $this->table = new Name(!$this->model ? $_table : call_user_func([$this->model, 'getTableName']), Name::NAME_TYPE_TABLE);
            }
        } else {
            throw new \SORM\Exception\BadFieldNameFormat($field);
        }
    }

    public function getTable() {
        return $this->table;
    }

    /**
     *
     * @return Name
     */
    public function getField() {
        return $this->field;
    }

    /**
     *
     * @return Name
     */
    public function getAlias() {
        return $this->alias;
    }

    public function setCoalesce($defaults, $cast = null) {
        if (!is_array($defaults)) {
            $defaults = [$defaults];
        }

        $this->defaults = $defaults;
        $this->defaultsCast = $cast;
    }

    public function addCast($cast) {
        $this->casts[] = $cast;
        return $this;
    }

    public function addFunction($function, $params = []) {
        $this->functions[] = [$function, $params];
        return $this;
    }

    public function setConcatSurround($string) {
        $this->concatSurround = $string;
    }

    public function __toString() {
        if (!$this->raw) {
            $name = "{$this->table}{$this->field}";
            if ($this->concatSurround != null) {
                $name = "'{$this->concatSurround}'||{$name}||'{$this->concatSurround}'";
            }

            if (isset($this->defaults)) {
                $defaults = $this->defaults;

                if ($this->defaultsCast != null) {
                    $name = "{$name}::{$this->defaultsCast}";
                }

                array_unshift($defaults, "{$name}");
                $defaults = implode(', ', $defaults);

                $name = "coalesce({$defaults})";
            }

            if (isset($this->functions) && !empty($this->functions)) {
                foreach ($this->functions as $function) {
                    $name = "\"{$function[0]}\"({$name})";
                }
            }

            if (isset($this->casts)) {
                foreach ($this->casts as $cast) {
                    $name = "({$name})::{$cast}";
                }
            }

            $this->raw = new Raw("{$name}{$this->alias}");
        }
        return (string) $this->raw;
    }

}
