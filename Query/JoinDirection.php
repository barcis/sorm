<?php

namespace SORM\Query;

/**
 * Description of JoinDirection
 *
 * @author barcis
 */
class JoinDirection {

    const JOIN_FULL = 'FULL OUTER';
    const JOIN_LEFT = 'LEFT OUTER';
    const JOIN_RIGHT = 'RIGHT OUTER';
    const JOIN_CROSS = 'CROSS';
    const JOIN_INNER = 'INNER';

    /**
     *
     * @param string $direction
     */
    public function __construct($direction = self::JOIN_INNER) {

        if ($direction instanceof Raw) {
            $this->raw = $direction;
            return;
        }

        if (!in_array($direction, [
                    self::JOIN_CROSS,
                    self::JOIN_FULL,
                    self::JOIN_INNER,
                    self::JOIN_LEFT,
                    self::JOIN_RIGHT])) {
            throw new \InvalidArgumentException('Bad argument from $direction!');
        }

        $this->direction = $direction;

        $this->raw = new Raw($this->direction);
    }

    public function __toString() {
        return (string) $this->raw;
    }

}
