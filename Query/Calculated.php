<?php

namespace SORM\Query;

/**
 * Description of Calculated
 *
 * @author barcis
 */
class Calculated {

    /**
     * string
     */
    private $expression;

    /**
     * string
     */
    private $alias;

    /**
     * string
     */
    private $table;

    /**
     * string
     */
    private $modifiers = [];

    public function __construct($expression, $alias, $table) {
        $this->expression = $expression;
        $this->alias = $alias;
        $this->table = $table;
    }

    public function getExpression() {
        return $this->expression;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function getTable() {
        return $this->table;
    }

    public function addModifier(callable $modifier) {
        $this->modifiers[] = $modifier;
        return $this;
    }

    /**
     *
     * @return callable
     */
    public function getModifiers() {
        return $this->modifiers;
    }

    public function getField($supquery) {
        if ($this->expression instanceof \SORM\Query) {
            $this->expression->setSupquery($supquery)
                    ->setAlias($this->alias);
//                    ->limit(1);
        }

        return new Field($this->expression, $this->table);
    }

}
