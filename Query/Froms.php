<?php

namespace SORM\Query;

/**
 * Description of Froms
 *
 * @author barcis
 */
class Froms extends \SORM\Pattern\Lists {

    private $withFrom;

    public function __construct($withFrom = true) {
        $this->withFrom = $withFrom;
    }

    public function __toString() {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }


        foreach ($items as $idx => $item) {
            if ($item instanceof \SORM\Query) {
                $items[$idx] = '(' . ((string) $item) . "){$item->getAlias()}";
            }
        }

        return ($this->withFrom ? '      FROM ' : '      , ') . implode(",\n", $items) . "\n";
    }

}
