<?php

namespace SORM\Query;

/**
 * Description of Name
 *
 * @author barcis
 */
class Name {

    /**
     * Raw
     */
    private $raw;

    /**
     *
     * @var string
     */
    private $name;

    /**
     * string
     */
    static private $q;

    const NAME_TYPE_FIELD = 0;
    const NAME_TYPE_ALIAS = 1;
    const NAME_TYPE_TABLE = 2;

    public function __construct($name, $type = NAME_TYPE_FIELD, $json = null, $col = null) {

        if (is_null($col)) {
            $col = '';
        }
        if (is_null($json)) {
            $json = '';
        } else {
            preg_match_all("((?<operator>[\-\#]\>{1,2})(?<field>[a-z0-9]+)+)", $json, $matches);
            $json = '';
            foreach ($matches['operator'] as $idx => $operator) {
                $json .= "{$operator}'{$matches['field'][$idx]}'";
            }
        }

        if (is_null(self::$q)) {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);
            /* @var $db Interfaces\Driver */
            self::$q = $db::FIELD_NAME_DELIMITER;
        }
        $q = self::$q;

        $this->name = $name;

        switch ($type) {
            case self::NAME_TYPE_ALIAS:
                $prepend = ' as ';
                $apend = '';
                break;

            case self::NAME_TYPE_TABLE:
                $prepend = '';
                $apend = '.';
                break;

            case self::NAME_TYPE_FIELD:
            default:
                $prepend = '';
                $apend = '';
                break;
        }

        $this->raw = new Raw("{$prepend}{$q}{$name}{$q}{$json}{$col}{$apend}");
    }

    public function isEmpty() {
        return empty($this->name);
    }

    public function getName() {
        return $this->name;
    }

    public function __toString() {
        if ($this->isEmpty()) {
            return '';
        }
        return (string) $this->raw;
    }

}
