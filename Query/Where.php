<?php

namespace SORM\Query;

/**
 * Description of Where
 *
 * @author barcis
 */
class Where {

    /**
     *
     * @var Raw
     */
    private $left;

    /**
     *
     * @var Raw
     */
    private $right;

    /**
     *
     * @var Raw
     */
    private $raw;
    private $operation;

    /**
     * string
     */
    static private $q;

    public function __construct($left, $operator = null, $right = null) {

        $argc = func_num_args();
        if (is_null(self::$q)) {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);
            /* @var $db Interfaces\Driver */
            self::$q = $db::FIELD_NAME_DELIMITER;
        }
        $q = self::$q;

        //p(['where:', 'left' => $left, 'sign' => $sign, 'right' => $right]);

        if ($left instanceof \Closure) {
            $query = new \SORM\Query();
            $left($query);
            $left = $query->getWheres();
        }

        $this->left = ($left instanceof Raw || $left instanceof Field || $left instanceof Wheres) ? $left : new Field($left);
        if (is_null($operator) && is_null($right)) {
            $this->raw = $this->left;
            return;
        } elseif (!is_null($operator) && is_null($right)) {
            try {
				
                $this->raw = new Operator($operator, $this->left, null);
            } catch (\Exception $ex) {
                $this->right = ($operator instanceof Raw || $operator instanceof Field || $operator instanceof Value || $operator instanceof \SORM\Query) ? $operator : new Value($operator);
                $this->raw = new Operator('=', $this->left, $this->right);
            }
        } elseif (!is_null($operator) && !is_null($right)) {
            if (($operator == 'IN' || $operator == 'NOT IN') && is_array($right) && count($right) === 0) {
                return;
            }


            $this->right = ($right instanceof Raw || $right instanceof Field || $right instanceof Value || $right instanceof \SORM\Query) ? $right : new Value($right);
            $this->raw = ($operator instanceof Raw || $operator instanceof Operator) ? $operator : new Operator($operator, $this->left, $this->right);
        } else {
            throw new \InvalidArgumentException("Bad arguments");
        }
    }

    public function setOperation($operation) {
        $this->operation = strtoupper($operation);
    }

    public function __toString() {
        if ($this->raw) {
            return ($this->operation ? str_pad($this->operation, 7, ' ', STR_PAD_RIGHT) : '') . $this->raw;
        }
        return '';
    }

}
