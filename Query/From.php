<?php

namespace SORM\Query;

/**
 * Description of From
 *
 * @author barcis
 */
class From {

    /**
     * string
     */
    private $model;

    /**
     * string
     */
    private $table;

    /**
     * string
     */
    private $alias;

    /**
     * Raw
     */
    private $raw;

    /**
     * string
     */
    static private $q;

    /**
     *
     * @var string
     */
    static private $regExpTable = '/^(?<table>[a-zA-Z0-9\_]+)((\ +[aA][sS])?\ +(?<alias>[a-zA-Z0-9\_]+))?$/';

    public function __construct($table, $alias = '') {

        if ($table instanceof Raw) {
            $this->raw = $table;
            return;
        } else if ($table instanceof \SORM\Query) {
            $sql = $table->getSql();
            $this->raw = "({$sql}) AS \"{$alias}\"";

            $this->model = $table->getFirstTableName()->getModel();

            $this->table = $table->getFirstTableName()->getTable();
            $this->alias = $table->getFirstTableName()->getAlias();
            return;
        }

        if (preg_match(self::$regExpTable, $table, $matches)) {

            $this->model = \SORM\Sorm::findModelByClassName($matches['table']);

            if (!$this->model) {
                $this->model = \SORM\Sorm::findModelByTableName($matches['table']);
            }

            $this->table = new Name(!$this->model ? $matches['table'] : call_user_func([$this->model, 'getTableName']), Name::NAME_TYPE_FIELD);
            $this->alias = new Name(!empty($matches['alias']) ? $matches['alias'] : $alias, Name::NAME_TYPE_ALIAS);
        } else {
            throw new \SORM\Exception\BadTableNameFormat($table);
        }

        if (is_null(self::$q)) {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);
            /* @var $db Interfaces\Driver */
            self::$q = $db::FIELD_NAME_DELIMITER;
        }
        $q = self::$q;

        $this->raw = new Raw("{$this->table}{$this->alias}");
    }

    public function getModel() {
        return $this->model;
    }

    public function getTable() {
        return $this->table;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function getColumns() {
        return call_user_func([$this->model, 'getColumns']);
    }

    public function getColumnsNames() {
        return call_user_func([$this->model, 'getColumnsNames']);
    }

    public function getColumnsNamesAutoSelect() {
        return call_user_func([$this->model, 'getColumnsNamesAutoSelect']);
    }

    /**
     *
     * @param string $name
     * @return Calculated
     */
    public function getCalculated($name) {
        return call_user_func([$this->model, 'getCalculated'], $name);
    }

    public function __toString() {
        return (string) $this->raw;
    }

}
