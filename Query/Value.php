<?php

namespace SORM\Query;

/**
 * Description of Value
 *
 * @author barcis
 */
class Value {

    /**
     * Raw
     */
    private $raw;
    private $type;

    /**
     * string
     */
    static private $q;

    private function conversion($type, $value) {
        $q = self::$q;
        switch ($type) {
            case 'boolean': $v = ($value ? 'TRUE' : 'FALSE');
                break;
            case 'double':
            case 'integer':
                $v = "{$value}";
                break;
            case 'array':
                $type = gettype($value[0]);
                $this->type = $type . '[]';
                $tmp = [];
                foreach ($value as $va) {
                    $tmp[] = $this->conversion(gettype($va), $va);
                }
                $v = "(" . implode(',', $tmp) . ")";
                break;
            case 'string':
            default:
                $value = pg_escape_string($value);
                $v = "{$q}{$value}{$q}";
        }
        return $v;
    }

    public function __construct($value) {

        if (is_null(self::$q)) {
            $config = \SORM\Sorm::getConnection('default');
            $db = \SORM\Factory\Driver::newInstance($config);
            /* @var $db Interfaces\Driver */
            self::$q = $db::STRING_DELIMITER;
        }
        $q = self::$q;

        $this->type = gettype($value);

        $v = $this->conversion($this->type, $value);

        // . " --({$this->type})"
        $this->raw = new Raw($v);
    }

    public function getType() {
        return $this->type;
    }

    public function __toString() {
        return (string) $this->raw;
    }

}
