<?php

namespace SORM\Query;

/**
 * Description of Wheres
 *
 * @author barcis
 */
class Wheres extends \SORM\Pattern\Lists {

    private $withWhere = false;

    public function setWithWhere($withWhere) {
        $this->withWhere = $withWhere === true;
    }

    public function __toString() {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }

        return ($this->withWhere ? "   WHERE " : "(") . implode("\n", $items) . ($this->withWhere ? "" : ")") . "\n";
    }

}
