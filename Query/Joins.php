<?php

namespace SORM\Query;

/**
 * Description of Joins
 *
 * @author barcis
 */
class Joins extends \SORM\Pattern\Lists {

    public function __toString() {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }

        return implode("", $items);
    }

}
