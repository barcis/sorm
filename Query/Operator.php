<?php

namespace SORM\Query;

/**
 * Description of Field
 *
 * @author barcis
 */
class Operator {

    /**
     * Raw
     */
    private $raw;

    public function center($op, $left, $right) {
        if ($right instanceof Value && $right->getType() === 'boolean') {
            $op = $op === '=' ? "is" : 'is not';
        } elseif ($right instanceof \SORM\Query) {
            $right = "(" . $right . ")";
        }

        $op = strtoupper($op);

        return "{$left} {$op} {$right}";
    }

    public function not($op, $left, $right) {
        return "not({$left})";
    }

    public function right($op, $left, $right) {
        $op = strtoupper($op);
        return "{$left} {$op}";
    }

    public function regexpquote(&$left, &$right) {
        if (!($right instanceof Raw)) {
            $right = preg_quote($right);
        }
    }

    static $operators = array(
        '=' => array(__CLASS__, 'center'),
        '!=' => array(__CLASS__, 'center'),
        '<>' => array(__CLASS__, 'center'),
        '>' => array(__CLASS__, 'center'),
        '<' => array(__CLASS__, 'center'),
        '>=' => array(__CLASS__, 'center'),
        '<=' => array(__CLASS__, 'center'),
        'like' => array(__CLASS__, 'center'),
        'not like' => array(__CLASS__, 'center'),
        'ilike' => array(__CLASS__, 'center'),
        'not ilike' => array(__CLASS__, 'center'),
        'similar' => array(__CLASS__, 'center'),
        'not similar' => array(__CLASS__, 'center'),
        'isimilar' => array(__CLASS__, 'center'),
        'not isimilar' => array(__CLASS__, 'center'),
        '~' => array(__CLASS__, 'center'),
        '!~' => array(__CLASS__, 'center'),
        '~*' => array(__CLASS__, 'center'),
        '!~*' => array(__CLASS__, 'center'),
        '!' => array(__CLASS__, 'not'),
        'not' => array(__CLASS__, 'not'),
        'is true' => array(__CLASS__, 'right'),
        'is not true' => array(__CLASS__, 'right'),
        'is false' => array(__CLASS__, 'right'),
        'is not false' => array(__CLASS__, 'right'),
        'is null' => array(__CLASS__, 'right'),
        'is not null' => array(__CLASS__, 'right'),
        'in' => array(__CLASS__, 'center'),
        'not in' => array(__CLASS__, 'center'),
    );
    static $translations = array(
        'similar' => '~',
        'not similar' => '!~',
        'isimilar' => '~*',
        'not isimilar' => '!~*'
    );
    static $modifiers = array(
        '~' => array(__CLASS__, 'regexpquote'),
        '!~' => array(__CLASS__, 'regexpquote'),
        '~*' => array(__CLASS__, 'regexpquote'),
        '!~*' => array(__CLASS__, 'regexpquote')
    );

    public function __construct($operator, $left, $right) {
        $operator = strtolower($operator);

        if (!isset(self::$operators[$operator])) {
            throw new \Exception("Bad operator '{$operator}'");
        }

        if (isset(self::$translations[$operator])) {
            $operator = self::$translations[$operator];
        }

        if (isset(self::$modifiers[$operator])) {
            call_user_func_array(self::$modifiers[$operator], [&$left, &$right]);
        }

        $this->raw = call_user_func_array(self::$operators[$operator], [$operator, $left, $right]);
    }

    public function __toString() {
        return $this->raw;
    }

}
