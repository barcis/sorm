<?php

namespace SORM\Query;

/**
 * Description of Field
 *
 * @author barcis
 */
class Fields extends \SORM\Pattern\Lists {

    public function __toString() {
        return implode(",\n           ", $this->getItems());
    }

}
