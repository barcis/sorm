<?php

namespace SORM\Query;

/**
 * Description of Joiner
 *
 * @method \SORM\Query where($where, $params) Foo
 * @method \SORM\Query whereIsNull($field) Foo
 * @method \SORM\Query whereIsNotNull($field) Foo
 * @method \SORM\Query whereIsTrue($field) Foo
 * @method \SORM\Query whereIsNotTrue($field) Foo
 * @method \SORM\Query whereIsFalse($field) Foo
 * @method \SORM\Query whereIsNotFalse($field) Foo
 * @method \SORM\Query whereIn($field) Foo
 * @method \SORM\Query whereNotIn($field) Foo
 * @method \SORM\Query whereLike($field) Foo
 * @method \SORM\Query whereNotLike($field) Foo
 * @method \SORM\Query whereSimilar($field) Foo
 * @method \SORM\Query whereNotSimilar($field) Foo
 * @method \SORM\Query whereILike($field) Foo
 * @method \SORM\Query whereNotILike($field) Foo
 * @method \SORM\Query whereISimilar($field) Foo
 * @method \SORM\Query whereNotISimilar($field) Foo
 * @method \SORM\Query whereNot($field) Foo
 *
 * @method \SORM\Query andWhere($where, $sing = null, $params = null) Foo
 * @method \SORM\Query andWhereIsNull($field) Foo
 * @method \SORM\Query andWhereIsNotNull($field) Foo
 * @method \SORM\Query andWhereIsTrue($field) Foo
 * @method \SORM\Query andWhereIsNotTrue($field) Foo
 * @method \SORM\Query andWhereIsFalse($field) Foo
 * @method \SORM\Query andWhereIsNotFalse($field) Foo
 * @method \SORM\Query andWhereIn($field) Foo
 * @method \SORM\Query andWhereNotIn($field) Foo
 * @method \SORM\Query andWhereLike($field) Foo
 * @method \SORM\Query andWhereNotLike($field) Foo
 * @method \SORM\Query andWhereSimilar($field) Foo
 * @method \SORM\Query andWhereNotSimilar($field) Foo
 * @method \SORM\Query andWhereILike($field) Foo
 * @method \SORM\Query andWhereNotILike($field) Foo
 * @method \SORM\Query andWhereISimilar($field) Foo
 * @method \SORM\Query andWhereNotISimilar($field) Foo
 * @method \SORM\Query andWhereNot($field) Foo
 *
 * @method \SORM\Query orWhere($where, $sing = null, $params = null) Foo
 * @method \SORM\Query orWhereIsNull($field) Foo
 * @method \SORM\Query orWhereIsNotNull($field) Foo
 * @method \SORM\Query orWhereIsTrue($field) Foo
 * @method \SORM\Query orWhereIsNotTrue($field) Foo
 * @method \SORM\Query orWhereIsFalse($field) Foo
 * @method \SORM\Query orWhereIsNotFalse($field) Foo
 * @method \SORM\Query orWhereIn($field) Foo
 * @method \SORM\Query orWhereNotIn($field) Foo
 * @method \SORM\Query orWhereLike($field) Foo
 * @method \SORM\Query orWhereNotLike($field) Foo
 * @method \SORM\Query orWhereSimilar($field) Foo
 * @method \SORM\Query orWhereNotSimilar($field) Foo
 * @method \SORM\Query orWhereILike($field) Foo
 * @method \SORM\Query orWhereNotILike($field) Foo
 * @method \SORM\Query orWhereISimilar($field) Foo
 * @method \SORM\Query orWhereNotISimilar($field) Foo
 * @method \SORM\Query orWhereNot($field) Foo
 *
 * @author barcis
 */
class Joiner {

    /**
     *
     * @var Raw
     */
    private $left;

    /**
     *
     * @var Raw
     */
    private $right;

    /**
     *
     * @var Raw
     */
    private $raw;
    private $operation;

    /**
     * string
     */
    static private $q;

    /**
     *
     * @var Where[]|Wheres
     */
    private $wheres;

    public function __construct($left, $operator = null, $right = null) {

        $this->wheres = new Wheres();
        $this->wheres->setWithWhere(false);

        if ($right === null) {
            $right = $operator;
            $operator = '=';
        }
        $left = ($left instanceof Raw || $left instanceof Field) ? $left : new Field($left);
        $right = ($left instanceof Raw || $right instanceof Field) ? $right : new Field($right);
        $this->addWhere('and', $left, $operator, $right);
    }

    public function __call($name, $arguments) {
        $whereOperatorsStr = implode("|", array_keys(\SORM\Query::$whereOperators));

        $regExp = "/^(?<operation>and|or)?(?<method>[Ww]here)(?<operator>{$whereOperatorsStr})?$/";

        if (preg_match($regExp, $name, $matches)) {
            $matches["method"] = strtolower($matches["method"]);

            if ($matches["method"] === 'where') {
                $operation = (isset($matches["operation"]) && !empty($matches["operation"])) ? $matches["operation"] : 'and';

                $where = isset($arguments[0]) ? $arguments[0] : null;
                $sing = isset($arguments[1]) ? $arguments[1] : null;
                $param = isset($arguments[2]) ? $arguments[2] : null;

                if (!isset($matches["operator"])) {
                    $this->addWhere($operation, $where, $sing, $param);
                } elseif (isset(\SORM\Query::$whereOperators[$matches["operator"]])) {
                    $opm = \SORM\Query::$whereOperators[$matches["operator"]];
                    $args = $opm($matches["operator"], $where, $sing, $param);
                    $this->addWhere($operation, $args[0], $args[1], $args[2]);
                } else {
                    throw new \BadMethodCallException($name);
                }
            }
        } else {
            throw new \BadMethodCallException($name);
        }

        return $this;
    }

    private function addWhere($operation, $where, $sing = null, $param = null) {
        if (!($where instanceof Where)) {
            $where = new Where($where, $sing, $param);
        }

        /* @var Query\Where $where */

        if (count($this->wheres) > 0) {
            $where->setOperation($operation);
        }

        $this->wheres[] = $where;

        return $this;
    }

    public function setOperation($operation) {
        $this->operation = strtoupper($operation);
    }

    public function __toString() {
        return ($this->operation ? str_pad($this->operation, 7, ' ', STR_PAD_RIGHT) : '') . $this->wheres;
    }

}
