<?php

namespace SORM\Query;

/**
 * Description of Raw
 *
 * @author barcis
 */
class Raw {

    /**
     * string
     */
    private $data;

    public function __construct($data) {
        $this->data = (string) $data;
    }

    public function __toString() {
        return $this->data;
    }

}
