<?php

namespace SORM\Query;

/**
 * Description of Joiners
 *
 * @author barcis
 */
class Joiners extends \SORM\Pattern\Lists {

    public function __toString() {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }

        return implode("\n             AND ", $items) . "\n";
    }

}
