<?php

namespace SORM\Query;

/**
 * Description of GroupBys
 *
 * @author barcis
 */
class GroupBys extends \SORM\Pattern\Lists {

    public function getString($withGroupBy = true) {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }

        return ($withGroupBy ? '  GROUP BY ' : '') . implode(",\n         ", $items) . "\n";
    }

    public function __toString() {
        return $this->getString(true);
    }

}
