<?php

namespace SORM\Query;

/**
 * Description of OrderBys
 *
 * @author barcis
 */
class OrderBys extends \SORM\Pattern\Lists {

    public function __toString() {
        $items = $this->getItems();
        if (empty($items)) {
            return '';
        }

        return '  ORDER BY ' . implode(",\n         ", $items) . "\n";
    }

}
