<?php

namespace SORM\Driver;

/**
 * Description of Pgsql
 *
 * @author adam
 */
class Pgsql extends \SORM\Driver {

    private $dns;
    private $link;
    private $debug = false;

    const STRING_DELIMITER = "'";
    const FIELD_NAME_DELIMITER = '"';

    public function __construct(\SORM\Config $config) {
        $this->dsn = 'host=' . $config->host . ' port=' . $config->port . ' dbname=' . $config->name . ' user=' . $config->user . ' password=' . $config->pass . ' options=\'--client_encoding=UTF8 \'';

        $this->link = @pg_connect($this->dsn, PGSQL_CONNECT_FORCE_NEW);
    }

    public function debug($debug) {
        $this->debug = $debug;
    }

    public function test() {
        return is_resource($this->link);
    }

    public function insert($sql, $targetClass = null) {
        $sql .= ' RETURNING id';

        if ($this->debug) {
            echo "\n\n{$sql}\n\n";
        }
        $res = $this->exec($sql, $targetClass);
        if (($data = pg_fetch_row($res))) {
            return $data[0];
        } else {
            return 0;
        }
    }

    public function update($sql) {
        return pg_affected_rows($this->exec($sql));
    }

    public function delete($sql) {
        return pg_affected_rows($this->exec($sql));
    }

    public function insertLater($sql) {
        $this->execLater($sql);
    }

    public function updateLater($sql) {
        $this->execLater($sql);
    }

    public function deleteLater($sql) {
        $this->execLater($sql);
    }

    public function exec($sql, $targetClass = null, $autocreate = true) {
        if ($this->debug) {
            echo "\n\n{$sql}\n\n";
        }

        $r = pg_query($this->link, $sql);
        if (!$r) {

            $code = $this->getErrorCode($message, $details);
            if ($code === '23505') {
                throw new \SORM\Exception\SqlUnique("Duplicated value", $code, $details);
            }
            $this->remakeTable($sql, $autocreate, $targetClass);
        }

        return $r;
    }

    public function execLater($sql) {
        return pg_send_query($this->link, $sql);
    }

    public function escape($string) {
        return pg_escape_string($string);
    }

    public function getSqlIdType() {
        return 'SERIAL';
    }

    public function getColumns($table) {
        $columnsSql = "
                SELECT column_name, data_type, character_maximum_length, column_default
                FROM information_schema.columns
                WHERE table_schema = 'public'
                  AND table_name   = '{$table}'";
        $result = pg_query($this->link, $columnsSql);
        return array_flip(pg_fetch_all_columns($result, 0));
    }

    public function query($sql, $targetClass = 'stdClass', $autocreate = true) {
        if ($this->debug) {
            echo "\n\n{$sql}\n\n";
        }

        $r = pg_query($this->link, $sql);

        if (!$r) {
            $this->remakeTable($sql, $autocreate, $targetClass);
        }

        $result = new \SORM\Models($sql);

        if ($targetClass != 'stdClass') {
            $btypes = array();
            $ccn = pg_num_fields($r);
            for ($ci = 0; $ci < $ccn; $ci++) {
                $n = pg_field_name($r, $ci);
                $btypes[$n] = pg_field_type($r, $ci);
            }

            $rnr = 0;

            $fnn = function($_btypes, $_rnr, $_r) {
                $__btypes = $_btypes;
                $__rnr = $_rnr;
                $__r = $_r;
                return function($obj) use ($__btypes, $__rnr, $__r) {
                    foreach ($__btypes as $n => $t) {
                        $nn = '"' . $n . '"';
                        if (pg_field_is_null($__r, $__rnr, $nn)) {
                            $obj->{$n} = null;
                        } elseif ($t === 'int4' || $t === 'int8') {
                            $obj->{$n} = (int) $obj->{$n};
                        } elseif ($t === 'numeric') {
                            $obj->{$n} = (float) $obj->{$n};
                        } elseif ($t === 'bool') {
                            $obj->{$n} = ($obj->{$n} == 't');
                        }
                    }
                };
            };


            while ($row = pg_fetch_object($r, null, $targetClass, ['sorm', null, $fnn($btypes, $rnr, $r)])) {
                $result[] = $row;
                $rnr++;
            }
        } else {
            while ($row = pg_fetch_object($r, null, $targetClass)) {
                $result[] = $row;
            }
        }
        pg_free_result($r);

        return $result;
    }

    private function getErrorCode(&$message = null, &$details = null) {
        $erroMsg = pg_last_error($this->link);

//        $m = var_export(preg_match('/^ERROR:  relation "[a-z0-9_]+" does not exist/', $erroMsg, $ma), true);
//        pd($erroMsg, $m, $ma);

        if (
                preg_match('/^BŁĄD:  relacja "[a-z0-9_]+" nie istnieje/', $erroMsg) ||
                preg_match('/^ERROR:  relation "[a-z0-9_]+" does not exist/', $erroMsg)
        ) {
            $code = '42P01';
        } elseif (preg_match('/^BŁĄD:  kolumna [a-z0-9_\.]+ nie istnieje/', $erroMsg) ||
                preg_match('/^ERROR:  column [a-z0-9_\.]+ does not exist/', $erroMsg)
        ) {
            $code = '42703';
        } elseif (preg_match('/^BŁĄD:  podwójna wartość klucza narusza ograniczenie unikalności/', $erroMsg)) {
            $code = '23505';
            $message = $erroMsg;
            $details = $erroMsg;
        } else {
            $code = -1 . ' ' . $erroMsg;
        }
        return $code;
    }

    private function remakeTable($sql, $autocreate, $targetClass) {
        $code = $this->getErrorCode();
        if ($code === '42703' /* UNDEFINED COLUMN */) {

            if ($autocreate) {
                $rfc = new \ReflectionClass($targetClass);
                $model = $rfc->newInstance();
                /* @var $model \SORM\Model */
                $table = $model->getTableName();

                $modelColumns = $model->getColumns();
                $tableColumns = $this->getColumns($table);

                $diff = false;
                foreach (array_keys($modelColumns) as $name) {
                    if (!isset($tableColumns[$name])) {
                        $diff = true;
                        break;
                    }
                }

                if ($diff) {
                    $replaceSQL = $model->getCreateSQL(true);
                    $r = pg_query($this->link, $replaceSQL);
                    if (!$r) {
                        $pgalter_error = pg_last_error($this->link);
                        pd($replaceSQL, $pgalter_error);
                    }
                }

                return $this->query($sql, $targetClass, false);
            }

            $pgerror = pg_last_error($this->link);

            throw new \SORM\Exception\SqlBadColumnName($code, '$column', $sql, $pgerror, []);
        } elseif ($code === '42P01' /* UNDEFINED TABLE */) {

            if ($autocreate) {
                $rfc = new \ReflectionClass($targetClass);
                $model = $rfc->newInstance();
                /* @var $model \SORM\Model */
                $replaceSQL = $model->getCreateSQL();
//                pd($replaceSQL);
                try {
                    pg_send_query($this->link, $replaceSQL);
                    $r = pg_get_result($this->link);
                    $code = pg_result_error_field($r, PGSQL_DIAG_SQLSTATE);
//                    pd($sql);
//                    pd($replaceSQL);
                    throw new \Exception('BŁĘDNY SQL DEFINICJI TABELI! ' . $code . ' ' . pg_last_error($this->link));
                } catch (\Exception $e) {
                    $text = "<br />\n==============================<br />\n";
                    $text .= "BŁĘDNY SQL DEFINICJI TABELI!<br />\n";
                    $text .= "Błędne zapytanie: <pre>{$replaceSQL}</pre><br />\n";
                    $text .= "Nadrzędne zapytanie: <pre>{$sql}</pre><br />\n";
                    $text .= "Treść błędu: " . pg_last_error($this->link) . "<br />\n";
                    $text .= "==============================<br />\n";
                    echo $text;
                    throw $e;
                }


                return $this->query($sql, $targetClass, false);
            }

            $text = "<br />\n==============================<br />\n";
            $text .= "BŁĘDNA NAZWA TABELI!<br />\n";
            $text .= "Błędne zapytanie: <pre>{$sql}</pre><br />\n";
            $text .= "Treść błędu: " . pg_last_error($this->link) . "<br />\n";
            $text .= "Kod błędu: {$code}<br />\n";
            $text .= "==============================<br />\n";
//            echo $text;
            throw new \Exception($text, $code);
        } elseif ($code != null) {

            $text = "<br />\n==============================<br />\n";
            $text .= "BŁĘDNE ZAPYTANIE!<br />\n";
            $text .= "Błędne zapytanie: <pre>{$sql}</pre><br />\n";
            $text .= "Treść błędu: " . pg_last_error($this->link) . "<br />\n";
            $text .= "Kod błędu: {$code}<br />\n";
            $text .= "==============================<br />\n";
//            echo $text;
            throw new \Exception($text, $code);
        }
    }

    public function getOne($q) {
        $r = pg_query($this->link, $q);
        if ($r === false) {
            echo 'błędne zapytanie: ' . ($q);
        }
        return pg_fetch_object($r);
    }

    public function getAll($q) {
        $r = pg_query($this->link, $q);
        $result = array();
        while ($row = pg_fetch_object($r)) {
            $result[] = $row;
        }
        return $result;
    }

    public function count($sql) {
        $r = pg_query($this->link, $sql);
        if ($r === false) {
            echo 'błędne zapytanie: ' . ($sql);
        }
        $row = pg_fetch_row($r);

        return (int) reset($row);
    }

    public function translateDbType($type) {
        switch ($type) {
            case 'int':
            case 'int unsigned':
                $value = '\SORM\Type\Integer';
                break;
            case 'varchar':
            case 'char':
            case 'text':
                $value = '\SORM\Type\String';
                break;
            default:
                $value = '\SORM\Type';
                break;
        }

        return $value;
    }

}
