<?php

namespace SORM\Driver;

/**
 * Description of Mysql
 *
 * @author barcis
 */
class Mysql extends \SORM\Driver {

    private $link;

    const STRING_DELIMITER = '"';
    const FIELD_NAME_DELIMITER = '`';

    public function __construct(\SORM\Config $config) {
        $this->link = mysql_connect($config->host, $config->user, $config->pass, true);
        mysql_select_db($config->name, $this->link);
        $this->exec("SET NAMES 'utf8'");
    }

    public function debug($debug) {
        ;
    }

    public function insert($sql) {
        $this->exec($sql);
        return mysql_insert_id();
    }

    public function update($sql) {
        $this->exec($sql);
        return mysql_affected_rows();
    }

    public function delete($sql) {
        $this->exec($sql);
        return mysql_affected_rows();
    }

    public function exec($sql) {
        $r = mysql_query($sql, $this->link);

        if ($r === false) {
            throw new Exception("MYSQL: błędne zapytanie: {$sql}<br/>\n" . mysql_error($this->link));
        }
    }

    public function escape($string) {
        return mysql_real_escape_string($string);
    }

    public function count($sql) {
        $r = mysql_query($sql, $this->link);
        if ($r === false) {
            echo 'błędne zapytanie: ' . ($sql);
        }
        $row = mysql_fetch_row($r);
        return $row[1];
    }

    public function query($sql, $targetClass = 'stdClass') {
        $r = mysql_query($sql, $this->link);
        $result = new \SORM\Models($sql, $targetClass);

        if ($r === false) {
            $error = mysql_error($this->link);
            $errno = mysql_errno($this->link);

            if ($errno === 1146) {
                throw new \Exception('błędne zapytanie: ' . ($sql) . '<br />' . 'Kod błędu: ' . $errno, 100001);
            } else {
                echo 'błędne zapytanie: ' . ($sql);
                echo $error . '[' . $errno . ']';
            }

            return false;
        }

        if ($targetClass != 'stdClass') {
            while ($row = mysql_fetch_object($r, $targetClass, array('sorm'))) {
                $result[] = $row;
            }
        } else {
            while ($row = mysql_fetch_object($r, $targetClass)) {
                $result[] = $row;
            }
        }
        mysql_free_result($r);

        return $result;
    }

    public function getOne($q) {
        $r = mysql_query($q, $this->link);
        if ($r === false) {
            echo 'błędne zapytanie: ' . ($q);
        }
        return mysql_fetch_object($r);
    }

    public function getAll($q) {
        $r = mysql_query($q, $this->link);
        $result = array();
        while ($row = mysql_fetch_object($r)) {
            $result[] = $row;
        }

        return $result;
    }

    public function translateDbType($type) {
        switch ($type) {
            case 'int':
            case 'int unsigned':
                $value = '\SORM\Type\Integer';
                break;
            case 'varchar':
            case 'char':
            case 'text':
                $value = '\SORM\Type\String';
                break;
            default:
                $value = '\SORM\Type';
                break;
        }

        return $value;
    }

}
