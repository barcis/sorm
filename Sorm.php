<?php

namespace SORM;

/**
 * Description of Sorm
 *
 * @author barcis
 */
class Sorm {

    /**
     *
     * @var \SORM\Config
     */
    private static $connections = array();
    protected $fromExtenders = null;
    protected $columns = null;
    protected $tables;
    protected $where = '';
    protected $limit;
    protected $offset;
    protected $calculated = array();
    protected $orderby = array();
    protected $groupby = array();

    /**
     * @var Interfaces\Driver
     */
    protected $db;
    protected $targetClass;

    public static function setConnection(string $name, \SORM\Config $cfg) {

        $cfgClon = clone $cfg;

        $cfgClon->setConnectionName($name);

        self::$connections[$name] = $cfgClon;
    }

    public static function setDefaultConnection(\SORM\Config $cfg) {
        self::setConnection('default', $cfg);
    }

    /**
     *
     * @param string $name
     * @return \SORM\Config
     */
    public static function getConnection(string $name = null) {
        if (empty($name)) {
            $name = 'default';
        }

        if (!isset(self::$connections[$name])) {
            throw new \Exception("Bad connection name '{$name}'.");
        }

        return self::$connections[$name];
    }

    /**
     *
     * @param string $name
     * @return \SORM\Config
     */
    public static function getModelsNamespaces(string $name = null) {
        return self::getConnection($name)->models;
    }

    public static function prepareModelNameFromTableName($name, $upperFirst = true) {
        $return = preg_replace_callback('/_([a-z])/', function($c) {
            return strtoupper($c[1]);
        }, $name);

        return $upperFirst ? ucfirst($return) : $return;
    }

    /**
     *
     * @param string $name
     * @return string|boolean
     */
    public static function findModelByClassName(string $name, string $connection = null) {
        $class = '\\' . ucfirst($name);

        foreach (self::getModelsNamespaces($connection) as $model) {
            if (class_exists($model . $class)) {
                return $model . $class;
            }
        }
        return false;
    }

    /**
     *
     * @param string $name
     * @return string|boolean
     */
    public static function findModelByTableName(string $name, string $connection = null) {
        $class = '\\' . self::prepareModelNameFromTableName($name, true);

        $tested = [];

        foreach (self::getModelsNamespaces($connection) as $model) {
            $tested[] = $model . $class;
            if (class_exists($model . $class)) {
                return $model . $class;
            }
        }
        return false;
    }

    public function __construct(Interfaces\Driver $db, $table, $targetClass = '\stdClass', $fromExtenders = null, $columns = null) {
        $this->targetClass = $targetClass;
        $this->tables = array($table);
        $this->fromExtenders = $fromExtenders;
        $this->columns = $columns;
        $this->db = &$db;
    }

    public function doQuery($fields = '*', $distinct = false) {
        if (count($this->groupby) > 0) {
            if (!is_array($fields) && !is_string($fields) || $fields == '*') {
                $fields = implode(',', $this->groupby) . ', count(1) AS count';
            }
        } else {
            if (!is_array($fields) && !is_string($fields)) {
                $fields = '*';
            } elseif (is_array($fields)) {
                $fields = implode(',', $fields) . ($distinct ? '' : ', T.id');
            } elseif (!is_array($fields) && $fields != 'count(1)') {
                $fields = $fields . ($distinct ? '' : ', T.id');
            }
        }

        if ($fields == '*' && $this->targetClass != 'stdClass') {
            $rfc = new \ReflectionClass($this->targetClass);
            $model = $rfc->newInstance();
            /* @var $model Model */
            $fields = 'T."' . implode('", T."', array_keys($model->getColumns())) . '"';
        }


        $query = "SELECT " . ($distinct ? 'DISTINCT ' : '') . "{$fields} FROM \"{$this->tables[0]}\" as T ";
        $tbs = $this->tables;
        unset($tbs[0]);

        foreach ($tbs as $table => $on) {
            $fromExtenders = array();
            $as = '';
            if (preg_match('/^\\\\/', $table, $maches)) {

                $class = $maches[1];
                $_table = $maches[2];
                $as = isset($maches[4]) ? $maches[4] : '';
                $rfc = new \ReflectionClass($class);

                if ($rfc->isSubclassOf('\\SORM\\Model')) {
                    $table = $_table;
                }
            } elseif (preg_match('/^([a-zA-Z0-9\_]+)(\ *[Aa][Ss])?\ *([a-zA-Z0-9]*)$/', $table, $maches)) {
                $table = $maches[1];
                $as = isset($maches[3]) ? $maches[3] : '';
            }
            $fromExtendersSQL = '';
            if (count($fromExtenders) > 0) {
                foreach ($fromExtenders AS $col => $value) {
                    $fromExtendersSQL .= ' AND ' . (strlen($as) > 0 ? $as . '.' : '') . '"' . $col . '" = ' . $value;
                }
            }

            $t = array_keys($on);
            $join_direction = reset($t);

            $query .= "\n " . $join_direction . ' JOIN "' . $table . '"' . (strlen($as) > 0 ? ' AS ' . $as : '') . ' ON (' . $on[$join_direction] . $fromExtendersSQL . ') ';
        }

        if (strlen($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        if ($this->fromExtenders != null) {
            if (strlen($this->where) == 0) {
                $query .= ' WHERE true';
            }

            foreach ($this->fromExtenders as $key => $value) {
                $query .= " AND T.{$key} = {$value}";
            }
        }

        if (count($this->groupby) > 0) {
            $query .= " GROUP BY " . implode(',', $this->groupby);
            ;
        }

        if (count($this->orderby) > 0) {
            $query .= " ORDER BY " . implode(',', $this->orderby);
        }

        if ($this->limit != null) {
            $query .= " LIMIT " . $this->limit;
        }
        if ($this->offset != null) {
            $query .= " OFFSET " . $this->offset;
        }
        return $query;
    }

    /**
     * @return Models
     */
    public function select($fields = null) {
        $query = $this->doQuery($fields);
        return $this->db->query($query, $this->targetClass);
    }

    public function selectDistinct($fields = null) {
        $query = $this->doQuery($fields, true);
        return $this->db->query($query, $this->targetClass);
    }

    /**
     * @return Models
     */
    public function simple($query) {
        return $this->db->query($query, $this->targetClass);
    }

    public function count() {
        $orderby = $this->orderby;
        $this->orderby = null;
        $query = $this->doQuery('count(1)');
        $this->orderby = $orderby;
        return $this->db->count($query);
    }

    /**
     *
     * @param type $fields
     * @return Model
     */
    public function one($fields = null) {
        $this->limit(1);
        $query = $this->doQuery($fields);

        $tmp = $this->db->query($query, $this->targetClass);

        if (!isset($tmp[0])) {
            return false;
        }
        return $tmp[0];
    }

    public function debug($debug) {
        $this->db->debug($debug);
    }

    public function getInsertSql(array $fields) {
        $names = array();
        $values = array();

        foreach ($fields as $name => $value) {
            if ($name == 'id') {
                continue;
            }
            $names[] = $name;

            if (is_null($value)) {
                $values[] = 'null';
            } else {
                if (is_array($this->columns) && isset($this->columns[$name])) {
                    $values[] = $this->columns[$name]->getFinalSqlValue($value);
                } else {
                    if (is_bool($value)) {
                        $values[] = $value ? 'true' : 'false';
                    } elseif (is_int($value)) {
                        $values[] = $this->db->escape($value);
                    } else {
                        $values[] = "'" . $this->db->escape($value) . "'";
                    }
                }
            }
        }

        $_names = implode('","', $names);
        $_values = implode(",", $values);

        $insertSql = "INSERT INTO \"{$this->tables[0]}\" (\"{$_names}\") VALUES ({$_values}) ";


        return $insertSql;
    }

    public function insert(array $fields) {
        return $this->db->insert($this->getInsertSql($fields), $this->targetClass);
    }

    public function getUpdateSql(array $fields) {
        $datas = array();

        foreach ($fields as $name => $value) {
            if ($name == 'id' || is_object($value)) {
                continue;
            }

            if (is_null($value)) {
                $SQLvalue = 'null';
            } else {
                if (is_array($this->columns) && isset($this->columns[$name])) {
                    $SQLvalue = $this->columns[$name]->getFinalSqlValue($value);
                } else {
                    if (is_bool($value)) {
                        $SQLvalue = $value ? 'true' : 'false';
                    } elseif (is_int($value)) {
                        $SQLvalue = $this->db->escape($value);
                    } else {
                        $SQLvalue = "'" . $this->db->escape($value) . "'";
                    }
                }
            }

            $datas[] = "\"$name\"={$SQLvalue}";
        }

        $_datas = implode(', ', $datas);

        $query = "UPDATE \"{$this->tables[0]}\" SET {$_datas}";

        if (strlen($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        return $query;
    }

    public function update(array $fields) {
        return $this->db->update($this->getUpdateSql($fields));
    }

    public function delete() {
        $query = "DELETE FROM \"{$this->tables[0]}\"";

        if (strlen($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        return $this->db->delete($query);
    }

    public function truncate() {
        $query = "TRUNCATE TABLE \"{$this->tables[0]}\"";
        return $this->db->exec($query);
    }

    /**
     * @param string $sql
     * @return type
     */
    public function exec($sql) {
        return $this->db->exec($sql);
    }

    /**
     * @param string $sql
     * @return type
     */
    public function query($sql) {
        return $this->db->query($sql);
    }

    /**
     *
     * @param array $join
     * @return Sorm
     */
    public function join($join, $condition = null) {
        if (!is_array($join) && $condition !== null) {
            $join = array($join => $condition);
        }

        foreach ($join as $table => $cond) {
            $this->tables[$table] = array('' => $cond);
        }

        return $this;
    }

    public function leftJoin(array $join) {
        $cond = reset($join);
        $table = key($join);
        $this->tables[$table] = array('LEFT' => $cond);
        return $this;
    }

    public function rightJoin(array $join) {
        $cond = reset($join);
        $table = key($join);
        $this->tables[$table] = array('RIGHT' => $cond);
        return $this;
    }

    /**
     *
     * @param array $join
     * @return Sorm
     */
    public function where($cond, $value = null) {
        $where = '';
        if ($cond === true || $cond === false) {
            $where = $cond ? 'TRUE' : 'FALSE';
        } elseif (is_array($cond)) {
            $where = "true ";
            foreach ($cond as $colDef => $val) {
                $tmp = explode('.', $colDef);
                if (count($tmp) == 1) {
                    $col = trim($tmp[0]);
                    $alias = '';
                } else {
                    $col = trim($tmp[1]);
                    $alias = trim($tmp[0]) . '.';
                }

                $where .= 'AND ' . $alias . '"' . $col . '" ';
                if (is_array($val)) {
                    $where .= "IN ('" . join("','", $val) . "') ";
                } else {
                    $where .= '= \'' . $val . '\' ';
                }
            }
        } elseif ((int) ($cond) > 0) {
            $where = '"id" = ' . $cond;
        } elseif (is_string($cond) && $value === null) {
            $where = $cond;
        } elseif (is_string($cond) && $value !== null) {
            $cond = trim($cond);

            $not = (substr($cond, 0, 1) === '!');
            if ($not) {
                $cond = substr($cond, 1);
            }

            $tmp = explode('.', $cond);
            if (count($tmp) == 1) {
                $col = trim($tmp[0]);
                $alias = '';
            } else {
                $col = trim($tmp[1]);
                $alias = trim($tmp[0]) . '.';
            }

            if (is_array($value) && count($value) > 0) {
                $where = $alias . '"' . $col . '" ' . ($not ? 'NOT' : '') . " IN ('" . join("','", $value) . "') ";
            } elseif (!is_array($value)) {
                $sign = '=';
                if (!preg_match('/[a-zA-Z0-9]/', substr($cond, 0, 1))) {
                    switch (substr($cond, 0, 1)) {
                        case '>':
                        case '<':
                            $sign = substr($cond, 0, 1);
                            if (substr($cond, 0, 2) == '=') {
                                $sign .= '=';
                            }
                            break;
                        case '~':
                            $sign = substr($cond, 0, 1);
                            if (substr($cond, 0, 2) == '*') {
                                $sign .= '*';
                            }
                            break;
                        default :
                            $sign = '=';
                            break;
                    }
                    $cond = substr($cond, 1);
                }

                $where = $alias . '"' . $col . '" ' . ($not ? '!=' : $sign) . ' \'' . $value . '\' ';
            }
        }

        $this->where = $where;
        return $this;
    }

    public function andWhere($cond, $value = null) {

        $tmp = $this->where . (strlen($this->where) > 0 ? ' AND ' : '');
        $this->where($cond, $value);
        $this->where = $tmp . $this->where;
        return $this;
    }

    public function addCalculated($expresion) {
        if (!is_callable()) {
            $expresion = function () use($expresion) {
                return $expresion;
            };
        }

        $this->calculated[] = $expresion;
    }

    public function orWhere($cond) {
        $this->where('(' . $this->where . ') OR (' . $cond . ')');
        return $this;
    }

    public function limit($limit) {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset) {
        $this->offset = $offset;
        return $this;
    }

    public function addOrderby($column, $direction = '') {
        $o = $this->orderby;
        $this->orderby($column, $direction);
        $this->orderby = array_merge($this->orderby, $o);
    }

    public function orderby($column, $direction = '') {
        switch (strtolower($direction)) {
            case 'asc':
            case 'desc':
                $direction = strtoupper($direction);
                break;

            default:
                $direction = '';
                break;
        }

        $this->orderby = array(trim($column . ' ' . $direction));
        return $this;
    }

    public function addGroupby($groupby) {
        $g = $this->groupby;
        $this->groupby($groupby);
        $this->groupby = array_merge($this->groupby, $g);
    }

    public function groupby($groupby) {
        $this->groupby = array($groupby);
        return $this;
    }

}
